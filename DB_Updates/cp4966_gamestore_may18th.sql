-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 18, 2020 at 12:41 PM
-- Server version: 10.3.23-MariaDB-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cp4966_gamestore`
--

-- --------------------------------------------------------

--
-- Table structure for table `cartitems`
--

CREATE TABLE `cartitems` (
  `id` int(11) NOT NULL,
  `sessionId` varchar(100) NOT NULL,
  `addedTS` timestamp NOT NULL DEFAULT current_timestamp(),
  `productId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unitPrice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cartitems`
--

INSERT INTO `cartitems` (`id`, `sessionId`, `addedTS`, `productId`, `quantity`, `unitPrice`) VALUES
(7, '3842ad42f5405069edea3b173f41f639', '2020-05-12 15:31:08', 2, 1, 0),
(8, '3842ad42f5405069edea3b173f41f639', '2020-05-12 15:31:11', 3, 1, 0),
(12, '3842ad42f5405069edea3b173f41f639', '2020-05-12 16:12:42', 11, 1, 0),
(13, '3842ad42f5405069edea3b173f41f639', '2020-05-12 16:15:40', 4, 1, 0),
(16, 'f3e3620b4d209d0addc6228f94360870', '2020-05-14 02:12:22', 11, 1, 0),
(17, 'f3e3620b4d209d0addc6228f94360870', '2020-05-14 02:20:55', 4, 1, 0),
(18, '7508416e6409fd646c78bddc1289a8b1', '2020-05-14 04:46:14', 11, 1, 50),
(19, '7508416e6409fd646c78bddc1289a8b1', '2020-05-14 04:48:07', 4, 1, 60),
(20, '9af06dc8da0222f3110cb42054ee8953', '2020-05-14 05:25:06', 4, 1, 60),
(27, 'a74ac571681d74263338e605b7f81bbd', '2020-05-15 01:51:18', 11, 1, 50),
(28, 'a74ac571681d74263338e605b7f81bbd', '2020-05-15 01:53:07', 9, 1, 25),
(29, 'a74ac571681d74263338e605b7f81bbd', '2020-05-15 01:53:10', 4, 1, 60),
(34, '9eac7dd812f370d19c1d501ffaf3bf49', '2020-05-17 18:51:49', 12, 1, 80),
(55, '82651b265dc0538da21d5c1ba43b6b96', '2020-05-18 19:32:51', 41, 1, 60);

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `id` int(11) NOT NULL,
  `orderId` varchar(255) NOT NULL,
  `productId` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `unitPrice` decimal(10,2) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderitems`
--

INSERT INTO `orderitems` (`id`, `orderId`, `productId`, `name`, `description`, `unitPrice`, `quantity`) VALUES
(3, 'ch_1Gih5PKXTVMqOXOqIF7bzQ1J', 12, 'Star Wars - Jedi Fallen Order (Xbox One)', 'Get your lightsaber ready with Star Wars Jedi: Fallen Order for Xbox One. Play the role of a Jedi Padawan who narrowly escaped the purge of Order 66 following the events of Revenge of the Sith. Pick up the pieces of your shattered past to complete your training, develop new powerful Force abilities, and master the art of the lightsaber.', 80.00, 1),
(4, 'ch_1GihRsKXTVMqOXOqOSvYGOrc', 4, 'Doom Eternal (PS4)', 'Doom Eternal for Playstation 4 is the direct sequel to the award winning original game DOOM. As the Doom slayer, you\'ll engage in first person combat as you seek your vengeance against the forces of evil. Slay demons with powerful new weapons and abilities in this pulse pounding thriller which allows you to fight across multiple dimensions.  ', 60.00, 1),
(5, 'ch_1GihRsKXTVMqOXOqOSvYGOrc', 6, 'FIFA 20 (XBox One)', 'EA SPORTS FIFA 20 for the Xbox One brings two sides of The World’s Game to life -- the prestige of the professional stage and an all-new authentic street football experience in EA SPORTS VOLTA. FOOTBALL INTELLIGENCE unlocks a platform for gameplay realism, FIFA Ultimate TeamTM offers more ways to build your dream squad, and EA SPORTS VOLTA returns the game to the street.', 55.00, 1),
(6, 'ch_1GihRsKXTVMqOXOqOSvYGOrc', 9, 'Moving Out (Xbox One)', 'Moving Out is a ridiculous physics-based moving simulator that brings new meaning to \"couch co-op\"! Are you ready for an exciting career in furniture? As a newly certified Furniture Arrangement & Relocation Technician, youâ€™ll take on moving jobs all across the busy town of Packmore.', 25.00, 1),
(8, 'ch_1Gk1rDKXTVMqOXOqeNvb8LQ8', 4, 'Doom Eternal (PS4)', 'Doom Eternal for Playstation 4 is the direct sequel to the award winning original game DOOM. As the Doom slayer, you\'ll engage in first person combat as you seek your vengeance against the forces of evil. Slay demons with powerful new weapons and abilities in this pulse pounding thriller which allows you to fight across multiple dimensions.  ', 60.00, 1),
(9, 'ch_1Gk1rDKXTVMqOXOqeNvb8LQ8', 5, 'The Last of Us Part II (PS4)', ' Five years after their dangerous journey across the post-pandemic United States, Ellie and Joel have settled down in Jackson, Wyoming. Living amongst a thriving community of survivors has allowed them peace and stability, despite the constant threat of the infected and other, more desperate survivors.\r\n\r\nWhen a violent event disrupts that peace, Ellie embarks on a relentless journey to carry out justice and find closure. As she hunts those responsible one by one, she is confronted with the devastating physical and emotional repercussions of her actions. ', 80.00, 1),
(10, 'ch_1Gk1rDKXTVMqOXOqeNvb8LQ8', 6, 'FIFA 20 (XBox One)', 'EA SPORTS FIFA 20 for the Xbox One brings two sides of The World’s Game to life -- the prestige of the professional stage and an all-new authentic street football experience in EA SPORTS VOLTA. FOOTBALL INTELLIGENCE unlocks a platform for gameplay realism, FIFA Ultimate TeamTM offers more ways to build your dream squad, and EA SPORTS VOLTA returns the game to the street.', 55.00, 1),
(11, 'ch_1Gk1rDKXTVMqOXOqeNvb8LQ8', 9, 'Moving Out (Xbox One)', 'Moving Out is a ridiculous physics-based moving simulator that brings new meaning to \"couch co-op\"! Are you ready for an exciting career in furniture? As a newly certified Furniture Arrangement & Relocation Technician, youâ€™ll take on moving jobs all across the busy town of Packmore.', 25.00, 1),
(12, 'ch_1Gk1rDKXTVMqOXOqeNvb8LQ8', 11, 'Spider-man (PS4)', 'Squeeze into your tights and put on your webshooters in Spider-Man Game of the Year Edition for PlayStation 4. But this isn\'t your average friendly neighbourhood Spider-Man. Now more mature and experienced, Peter Parker has become an expert crime fighter with great power who uses parkour, webslinging, and engages in Hollywood-style combat.', 50.00, 1),
(13, 'ch_1Gk1rDKXTVMqOXOqeNvb8LQ8', 37, 'Desperados III (Xbox One)', 'Journey to a modern-day Wild West in Desperados III for Xbox One. This real-time game puts you in charge of a group of unlikely heroes and heroines who are hunted by vicious bandits and corrupt lawmen. Travel through frontier towns and modern cities and use the skills of the Desperados to take out enemies, complete missions, and execute attacks.', 55.00, 1),
(14, 'ch_1Gk2BJKXTVMqOXOqQsUcZZKm', 7, 'Minecraft Dungeons (PC)', 'Minecraft Dungeons. An all-new action-adventure game, inspired by classic dungeon crawlers and set in the Minecraft universe! Brave the dungeons alone, or team up with friends! Up to four players can battle together online or in couch co-op. \r\n\r\nUse melee swings, hang back with ranged attacks, or tank your way through shielded by heavy armour! Personalize your character and unlock unique items and weapon enchantments for devastating special attacks. Explore the action-packed, treasure-stuffed levels – all in an epic quest to save the villagers and take down the evil Arch-Illager!', 40.00, 1),
(15, 'ch_1Gk2BJKXTVMqOXOqQsUcZZKm', 35, 'Cyberpunk 2077 (Xbox One)', 'Cyberpunk 2077 is a science fiction game loosely based on the role-playing game Cyberpunk 2020. Setting The game is set in the year 2077 in a fictional futuristic metropolis Night City in California. In the world of the game, there are developed cybernetic augmentations that enhance people\'s strength, agility, and memory. The city is governed by corporations. Many jobs are taken over by the robots, leaving a lot of people poor and homeless. ', 80.00, 1),
(16, 'ch_1Gk2BJKXTVMqOXOqQsUcZZKm', 36, 'SpongeBob SquarePants: Battle for Bikini Bottom Rehydrated (Switch)', 'Head down to your favourite pineapple under the sea in SpongeBob SquarePants: Battle for Bikini Bottom - Rehydrated for Nintendo Switch. Play as SpongeBob, Patrick, or Sandy and foil Plankton\'s evil plan to take over Bikini Bottoms with an army of robots. This remake of a cult classic has been beautifully updated with high-end visuals and modern gameplay.', 30.00, 1),
(17, 'ch_1Gk2BJKXTVMqOXOqQsUcZZKm', 38, 'Ghost of Tsushima Launch Edition (PS 4)', 'Defend Japan from a massive Mongol invasion in Ghost of Tsushima Launch Edition for PlayStation 4. In this open-world action-adventure epic, Jin Sakai wages an unconventional war to reclaim his home, Tsushima Island, from the Mongol empire in the late 13th century. To do this, he must break from samurai traditions and forge a new path to become the Ghost.', 80.00, 1),
(18, 'ch_1Gk2BJKXTVMqOXOqQsUcZZKm', 39, 'The Legend of Heroes: Trails of Cold Steel III (Switch)', 'Rean Schwarzer uncovers a dark plot that threatens his homeland. To face their enemies, he must prepare a new generation of heroes as an instructor at a new branch campus and guide them towards victory.', 80.00, 1),
(19, 'ch_1Gk2BJKXTVMqOXOqQsUcZZKm', 40, 'Marvel\'s Avengers (Xbox One)', 'Play as Earth\'s mightiest heroes in Marvel\'s Avengers. You can assume the role of Captain America, Thor, Hulk, Black Widow, and Iron Man in a new, original story. After a catastrophic accident the Avengers become outlaws, but have to come back together when the world faces a terrible threat.', 80.00, 1),
(20, 'ch_1Gk2LoKXTVMqOXOqkG13nbLD', 26, 'God of War (PS 4)', 'Join the god-slaying Spartan once again on a quest of survival in the God of War video game for PlayStation 4. Kratos puts his past behind and starts a new life of mentoring his son Atreus to prevail in the dangerous Norse world. Explore the extensive lands, master new abilities, and battle strong opponents in an action-packed gameplay.', 20.00, 1),
(21, 'ch_1Gk2LoKXTVMqOXOqkG13nbLD', 32, 'NFS Heat (PS 4)', 'Go head to head against a rogue police force in Need for Speed Heat for PlayStation 4. Battle your way into street racing\'s elite by competing in the Speedhunter Showdown where you\'ll earn money to customize and upgrade your high-performance cars. Then, head into the night to compete in illicit street races while you outwit the dishonest cops who want to take you down.', 40.00, 1),
(22, 'ch_1Gk2LoKXTVMqOXOqkG13nbLD', 34, 'Borderlands 3 (PS 4)', 'The original shooter-looter returns, packing bazillions of guns and an all-new mayhem-fueled adventure in Borderlands 3 for PlayStation 4. Blast through new worlds and enemies as one of four brand new treasure-seeking Vault Hunters, each with deep skill trees, abilities, and customization. Play solo or join with friends to take on insane enemies.', 30.00, 1),
(23, 'ch_1Gk2LoKXTVMqOXOqkG13nbLD', 41, 'Tony Hawk\'s Pro Skater 1 + 2 (PS 4)', 'Grab your deck and griptape and get ready to drop in for a pair of the greatest skateboarding games ever made. Tony Hawk\'s Pro Skater 1 + 2 brings both games to one collection on PlayStation 4, remastered in HD and rebuilt from the ground up. Play as your favourite pro skaters, with all the classic levels and tricks in stunning new detail.', 60.00, 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `firstLastName` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `postalCode` varchar(10) NOT NULL,
  `phoneNo` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `orderDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `deliveryMethod` enum('PickUp','Delivery') NOT NULL,
  `deliveryCost` decimal(10,2) NOT NULL,
  `dueDate` datetime NOT NULL,
  `totalAmountBeforeTax` decimal(10,2) NOT NULL,
  `taxAmount` decimal(10,2) NOT NULL,
  `totalPaid` decimal(10,2) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `orderStatus` enum('Placed','Shipped','Delivered','Returned') NOT NULL,
  `userSCid` varchar(255) NOT NULL,
  `paymentStatus` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `userId`, `firstLastName`, `address`, `postalCode`, `phoneNo`, `email`, `orderDate`, `deliveryMethod`, `deliveryCost`, `dueDate`, `totalAmountBeforeTax`, `taxAmount`, `totalPaid`, `currency`, `orderStatus`, `userSCid`, `paymentStatus`) VALUES
('ch_1Gih5PKXTVMqOXOqIF7bzQ1J', 24, 'Tom McTom', '3450 Durocher', 'H2X 2E1', '4388201436', 'tom@mail.com', '2020-05-14 13:26:56', 'Delivery', 10.00, '2020-05-14 13:26:54', 79.99, 13.50, 103.49, 'cad', 'Placed', 'cus_HHFaX7PfXcSV81', 'succeeded'),
('ch_1GihRsKXTVMqOXOqOSvYGOrc', 19, 'gi ou', '123 My Street', 'H3T 1E7', '4564522525', 'giovanaourique@gmail.com', '2020-05-14 13:50:09', 'Delivery', 10.00, '2020-05-14 13:50:07', 139.98, 22.50, 172.48, 'cad', 'Placed', 'cus_HHFxmfbRGRZba4', 'succeeded'),
('ch_1GiVqsKXTVMqOXOq22BExYso', 6, 'Tom Admin', '321 Main St', 'H2X 2E1', '4388201436', 'tom@mail.com', '2020-05-14 01:27:11', 'Delivery', 10.00, '2020-05-14 03:27:09', 129.99, 21.00, 160.99, 'cad', 'Placed', 'cus_HH3yNcYlRzz7jB', 'succeeded'),
('ch_1GiVwmKXTVMqOXOq2Tvm4wPp', 6, 'Tom Admin', '321 Main St', 'H2X 2E1', '4388201436', 'tom@mail.com', '2020-05-14 01:33:16', 'Delivery', 10.00, '2020-05-14 03:33:15', 154.99, 24.75, 189.74, 'cad', 'Placed', 'cus_HH44jwukbHRvKF', 'succeeded'),
('ch_1GiWHJKXTVMqOXOqDt9MY9vJ', 6, 'Tom Admin', '321 Mi Street', 'H2X 2E1', '4388201436', 'tom@mail.com', '2020-05-14 01:54:29', 'Delivery', 10.00, '2020-05-14 03:54:27', 194.98, 30.75, 235.73, 'cad', 'Placed', 'cus_HH4Qc6lGrkn7DP', 'succeeded'),
('ch_1GiX1BKXTVMqOXOqF0Fe8DBq', 6, 'TOm Smith', '3450 Durocher', 'H2X 2E1', '4388201436', 'tom@mail.com', '2020-05-14 02:41:53', 'Delivery', 10.00, '2020-05-14 04:41:52', 179.99, 28.50, 218.49, 'cad', 'Placed', 'cus_HH5BV3edU6AlKX', 'succeeded'),
('ch_1GiXF7KXTVMqOXOqaqsWufOS', 6, 'Tom Smith-Smith', '321 Main St', 'H2X 2E1', '4388201436', 'tom@mail.com', '2020-05-14 02:56:18', 'Delivery', 10.00, '2020-05-14 04:56:16', 79.99, 13.50, 103.49, 'cad', 'Placed', 'cus_HH5PDQQOVT2Mkx', 'succeeded'),
('ch_1GiXJrKXTVMqOXOqjnC0Cq77', 6, 'Tom Smith-Smith', '3450 Durocher', 'H2X 2E1', '4388201436', 'tom@mail.com', '2020-05-14 03:01:11', 'Delivery', 10.00, '2020-05-14 05:01:10', 139.99, 22.50, 172.49, 'cad', 'Placed', 'cus_HH5UHhdZrMK0ut', 'succeeded'),
('ch_1Gk1rDKXTVMqOXOqeNvb8LQ8', 19, 'Giovana', '123 My Street', 'H4H5H6', '2514445555', 'giovanaourique@gmail.com', '2020-05-18 05:49:47', 'Delivery', 10.00, '2020-05-18 05:49:46', 324.95, 50.24, 385.19, 'cad', 'Placed', 'cus_HId7Cp0IYhhvcL', 'succeeded'),
('ch_1Gk2BJKXTVMqOXOqQsUcZZKm', 18, 'Giovana', '123 My Street', 'H4H5H6', '2514445555', 'giovana.ourique@hotmail.com', '2020-05-18 06:10:33', 'Delivery', 10.00, '2020-05-18 06:10:32', 389.94, 59.99, 459.93, 'cad', 'Placed', 'cus_HIdSmTg6UGENsM', 'succeeded'),
('ch_1Gk2LoKXTVMqOXOqkG13nbLD', 5, 'Maria', '123 My Street', 'H4H5H6', '2514445555', 'maria@mail.com', '2020-05-18 06:21:24', 'Delivery', 10.00, '2020-05-18 06:21:23', 149.96, 23.99, 183.95, 'cad', 'Placed', 'cus_HIddMooMV35Sly', 'succeeded');

-- --------------------------------------------------------

--
-- Table structure for table `passwordresets`
--

CREATE TABLE `passwordresets` (
  `userId` int(11) NOT NULL,
  `secret` varchar(200) NOT NULL,
  `creationDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `passwordresets`
--

INSERT INTO `passwordresets` (`userId`, `secret`, `creationDateTime`) VALUES
(18, 'IFYVgYekBIZxiScDQcFagufE86zoXSOVnY2gqgsUVZCRctjrZpwvWYScKLjs', '2020-05-18 14:52:31');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category` enum('Games','Consoles','Acessories') NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `game_genre` enum('Action','Adventure','Arcade','Dance','Fighting','Horror','Party','Puzzle','Racing','Role-Playing','RPG','Sci-Fi','Shooters','Simulation','Sports','Strategy') DEFAULT NULL,
  `developer` enum('Nintendo','Microsoft','Sony','Bethesda','Ubisoft','EA','Other','Atari','NaughtyDog') NOT NULL,
  `platform` enum('Nintendo DS','Nintendo 3DS & 2DS','Nintendo Switch','PC','PlayStation 3','PlayStation 4','PlayStation Vita','PlayStation VR','Xbox 360','Xbox One','Wii','Wii U') NOT NULL,
  `releaseDate` date NOT NULL,
  `unitPrice` decimal(10,2) NOT NULL,
  `onSale` enum('yes','no') NOT NULL,
  `newPrice` decimal(10,2) DEFAULT NULL,
  `video` varchar(250) DEFAULT NULL,
  `picture1` varchar(250) DEFAULT NULL,
  `picture2` varchar(250) DEFAULT NULL,
  `picture3` varchar(250) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category`, `name`, `description`, `game_genre`, `developer`, `platform`, `releaseDate`, `unitPrice`, `onSale`, `newPrice`, `video`, `picture1`, `picture2`, `picture3`, `url`) VALUES
(1, 'Games', 'Super Mario Odyssey (Switch)', 'Join Mario on a massive, globe-trotting 3D adventure and use his incredible new abilities to collect Moons so you can power up your airship, the Odyssey, and rescue Princess Peach from Bowser\'s wedding plans! This sandbox-style 3D Mario adventure—the first since 1996\'s beloved Super Mario 64 and 2002\'s Nintendo GameCube classic Super Mario Sunshine—is packed with secrets and surprises, and with Mario\'s new moves like cap throw, cap jump, and capture, you\'ll have fun and exciting gameplay experiences unlike anything you\'ve enjoyed in a Mario game before. Get ready to be whisked away to strange and amazing places far from the Mushroom Kingdom!', 'Adventure', 'Nintendo', 'Nintendo Switch', '2017-10-27', 79.99, 'no', 79.99, NULL, 'product_pictures/MarioOdyssey/MarioOdyssey_01.jpg', 'product_pictures/MarioOdyssey/MarioOdyssey_02.jpg', 'product_pictures/MarioOdyssey/MarioOdyssey_03.jpg', 'https://www.youtube.com/watch?v=wGQHQc_3ycE'),
(2, 'Games', 'The Legend of Zelda: Breath of the Wild (Switch)', 'Forget everything you know about The Legend of Zelda games. Step into a world of discovery, exploration, and adventure in The Legend of Zelda: Breath of the Wild, a boundary-breaking new game in the acclaimed series. Travel across vast fields, through forests, and to mountain peaks as you discover what has become of the kingdom of Hyrule in this stunning Open-Air Adventure. Now on Nintendo Switch, your journey is freer and more open than ever. Take your system anywhere, and adventure as Link any way you like.', 'RPG', 'Nintendo', 'Nintendo Switch', '2017-03-03', 79.99, 'no', 79.99, NULL, 'product_pictures/ZeldaBOTW/ZeldaBOTW_01.jpg', 'product_pictures/ZeldaBOTW/ZeldaBOTW_02.jpg', 'product_pictures/ZeldaBOTW/ZeldaBOTW_03.jpg', 'https://www.youtube.com/watch?v=zw47_q9wbBE'),
(3, 'Games', 'Animal Crossing: New Horizons (Switch)', 'Escape to a deserted island and create your own paradise as you explore, create, and customize in the Animal Crossing: New Horizons game. Your island getaway has a wealth of natural resources that can be used to craft everything from tools to creature comforts. You can hunt down insects at the crack of dawn, decorate your paradise throughout the day, or enjoy sunset on the beach while fishing in the ocean. The time of day and season match real life, so each day on your island is a chance to check in and find new surprises all year round.\r\n\r\nShow off your island utopia to family and friends—or pack your bags and visit theirs. Whether playing online* or with others beside you**, island living is even better when you can share it. Even without hopping on a flight, you’ll meet a cast of charming animal residents bursting with personality. Friendly faces like Tom Nook and Isabelle will lend their services and happily help you grow your budding community. Escape to your island getaway—however, whenever, and wherever you want.', 'Simulation', 'Nintendo', 'Nintendo Switch', '2020-03-20', 79.99, 'no', 79.99, NULL, 'product_pictures/AnimalCrossing/AnimalCrossing_01.jpg', 'product_pictures/AnimalCrossing/AnimalCrossing_02.jpg', 'product_pictures/AnimalCrossing/AnimalCrossing_03.jpg', 'https://www.youtube.com/watch?v=sRWjpjNVOCM'),
(4, 'Games', 'Doom Eternal (PS4)', 'Doom Eternal for Playstation 4 is the direct sequel to the award winning original game DOOM. As the Doom slayer, you\'ll engage in first person combat as you seek your vengeance against the forces of evil. Slay demons with powerful new weapons and abilities in this pulse pounding thriller which allows you to fight across multiple dimensions.  ', 'Shooters', 'Bethesda', 'PlayStation 4', '2020-03-20', 59.99, 'no', 59.99, NULL, 'product_pictures/DOOMEternal/DOOMEternal_01.jpg', 'product_pictures/DOOMEternal/DOOMEternal_02.jpg', 'product_pictures/DOOMEternal/DOOMEternal_03.jpg', 'https://www.youtube.com/watch?v=qgvV4GE8vVA'),
(5, 'Games', 'The Last of Us Part II (PS4)', ' Five years after their dangerous journey across the post-pandemic United States, Ellie and Joel have settled down in Jackson, Wyoming. Living amongst a thriving community of survivors has allowed them peace and stability, despite the constant threat of the infected and other, more desperate survivors.\r\n\r\nWhen a violent event disrupts that peace, Ellie embarks on a relentless journey to carry out justice and find closure. As she hunts those responsible one by one, she is confronted with the devastating physical and emotional repercussions of her actions. ', 'Action', 'NaughtyDog', 'PlayStation 4', '2020-05-29', 79.99, 'no', 79.99, '', 'product_pictures/LastOfUsII/LastOfUsII_01.jpeg', 'product_pictures/LastOfUsII/LastOfUsII_02.jpg', 'product_pictures/LastOfUsII/LastOfUsII_03.jpg', 'https://www.youtube.com/watch?v=X0VubwgS2Y4'),
(6, 'Games', 'FIFA 20 (XBox One)', 'EA SPORTS FIFA 20 for the Xbox One brings two sides of The World’s Game to life -- the prestige of the professional stage and an all-new authentic street football experience in EA SPORTS VOLTA. FOOTBALL INTELLIGENCE unlocks a platform for gameplay realism, FIFA Ultimate TeamTM offers more ways to build your dream squad, and EA SPORTS VOLTA returns the game to the street.', 'Sports', 'EA', 'Xbox One', '2019-09-24', 54.99, 'yes', 24.99, NULL, 'product_pictures/FIFA20//FIFA20_01.jpg', 'product_pictures/FIFA20//FIFA20_02.jpg', 'product_pictures/FIFA20//FIFA20_03.jpg', 'https://www.youtube.com/watch?v=vgQNOIhRsV4'),
(7, 'Games', 'Minecraft Dungeons (PC)', 'Minecraft Dungeons. An all-new action-adventure game, inspired by classic dungeon crawlers and set in the Minecraft universe! Brave the dungeons alone, or team up with friends! Up to four players can battle together online or in couch co-op. \r\n\r\nUse melee swings, hang back with ranged attacks, or tank your way through shielded by heavy armour! Personalize your character and unlock unique items and weapon enchantments for devastating special attacks. Explore the action-packed, treasure-stuffed levels – all in an epic quest to save the villagers and take down the evil Arch-Illager!', 'Adventure', 'Other', 'PC', '2020-05-26', 39.99, 'no', 39.99, NULL, 'product_pictures/MinecraftDungeons/MinecraftDungeons_01.jpg', 'product_pictures/MinecraftDungeons/MinecraftDungeons_02.png', 'product_pictures/MinecraftDungeons/MinecraftDungeons_03.jpg', 'https://www.youtube.com/watch?v=mJczpIdONjs'),
(8, 'Consoles', 'Nintendo Switch', 'Get the gaming system that lets you play the games you want, wherever you are, however you like.\r\n\r\nIncludes the Nintendo Switch console and Nintendo Switch dock in black, with contrasting left and right Joy‑Con controllers—one red, one blue. Also includes all the extras you need to get started.\r\n\r\nModel number: HAC-001(-01) (product serial number begins with “XKW”)\r\n\r\nThis model includes battery life of approximately 4.5 - 9 hours*.\r\n\r\n*The battery life will depend on the games you play. For instance, the battery will last approximately 5.5 hours for The Legend of Zelda: Breath of the Wild.', NULL, 'Nintendo', 'Nintendo Switch', '2017-03-03', 399.99, 'no', 399.99, NULL, 'product_pictures/NintendoSwitch/NintendoSwitch_01.jpg', 'product_pictures/NintendoSwitch/NintendoSwitch_02.jpg', 'product_pictures/NintendoSwitch/NintendoSwitch_03.jpg', 'https://www.youtube.com/watch?v=f5uik5fgIaI'),
(9, 'Games', 'Moving Out (Xbox One)', 'Moving Out is a ridiculous physics-based moving simulator that brings new meaning to \"couch co-op\"! Are you ready for an exciting career in furniture? As a newly certified Furniture Arrangement & Relocation Technician, youâ€™ll take on moving jobs all across the busy town of Packmore.', 'Simulation', 'Other', 'Xbox One', '2020-04-28', 25.00, 'no', 25.00, '', 'product_pictures/MovingOutTest2/movingout_01.jpeg', 'product_pictures/MovingOutTest2/movingout_02.jpg', 'product_pictures/MovingOutTest2/movingout_03.jpg', 'https://www.youtube.com/watch?v=7GmIvduEzP8'),
(11, 'Games', 'Spider-man (PS4)', 'Squeeze into your tights and put on your webshooters in Spider-Man Game of the Year Edition for PlayStation 4. But this isn\'t your average friendly neighbourhood Spider-Man. Now more mature and experienced, Peter Parker has become an expert crime fighter with great power who uses parkour, webslinging, and engages in Hollywood-style combat.', 'Action', 'Other', 'PlayStation 4', '2018-09-07', 49.99, 'no', 49.99, NULL, 'product_pictures/Spiderman/spider-man_01.jpg', 'product_pictures/Spiderman/spider-man_02.jpg', 'product_pictures/Spiderman/spider-man_04.jpeg', 'https://www.youtube.com/watch?v=b5-_MvCWSfI'),
(12, 'Games', 'Star Wars - Jedi Fallen Order (Xbox One)', 'Get your lightsaber ready with Star Wars Jedi: Fallen Order for Xbox One. Play the role of a Jedi Padawan who narrowly escaped the purge of Order 66 following the events of Revenge of the Sith. Pick up the pieces of your shattered past to complete your training, develop new powerful Force abilities, and master the art of the lightsaber.', 'Action', 'EA', 'Xbox One', '2019-11-15', 79.99, 'no', 79.99, NULL, 'product_pictures/SW-JediFallenOrder/swjedifo_01.jpg', 'product_pictures/SW-JediFallenOrder/swjedifo_02.jpg', 'product_pictures/SW-JediFallenOrder/swjedifo_03.jpg', 'https://www.youtube.com/watch?v=0GLbwkfhYZk'),
(15, 'Games', 'Mario Kart 8 Deluxe (Switch)', 'Race your friends on 48 tracks with Mario Kart 8 Deluxe on Nintendo Switch. Choose your favourite character and take on your opponents with 5 types of battles. This game features every course from the Wii U version, including DLC, and many more including ones inspired by Excite Bike and Legend of Zelda. The 200cc Time Trials mode allows you to drive even faster.', 'Racing', 'Nintendo', 'Nintendo Switch', '2017-04-27', 79.99, 'no', 79.99, '', 'product_pictures/MarioKart8Deluxe/mkart8.jpg', 'product_pictures/MarioKart8Deluxe/mkart81.jpg', 'product_pictures/MarioKart8Deluxe/mkart82.jpg', 'https://www.youtube.com/watch?v=tKlRN2YpxRE'),
(16, 'Games', 'NBA 2K20 (PS4)', 'NBA 2K20 for PlayStation 4 continues to redefine what is possible in sports gaming, featuring jaw dropping graphics and gameplay, ground breaking game modes, and unparalleled player control and customization.', 'Sports', 'Other', 'PlayStation 4', '2019-09-05', 19.99, 'no', 19.99, '', 'product_pictures/NBA2K20/NBA2K20_01.jpg', 'product_pictures/NBA2K20/NBA2K20_02.jpg', 'product_pictures/NBA2K20/NBA2K20_03.jpg', 'https://www.youtube.com/watch?v=FQ7WBnSvjIo'),
(17, 'Games', 'Call of Duty: Modern Warfare (Xbox One)', 'Call of Duty: Modern Warfare returns on Xbox One with a raw, provocative narrative that delivers intense, action-packed gameplay. Players are immersed in a global conflict that requires sharp wits, clever strategy, and tough choices. Choose from the heart-pounding single-player mode, the classic online multiplayer option, or the elite co-op missions.', 'Shooters', 'Other', 'Xbox One', '2019-10-25', 79.99, 'no', 79.99, '', 'product_pictures/CallOfDutyMW/CallOfDutyMW_01.jpg', 'product_pictures/CallOfDutyMW/CallOfDutyMW_02.jpg', 'product_pictures/CallOfDutyMW/CallOfDutyMW_03.jpg', 'https://www.youtube.com/watch?v=bH1lHCirCGI'),
(18, 'Games', 'Luigi\'s Mansion 3 (Switch)', 'Luigi embarks on a dream vacation with Mario and friends upon receiving an invitation to a luxurious hotel. His dream quickly becomes a nightmare when King Boo reveals it\'s a ploy to capture Mario and friends. With the assistance of Professor E. Gadd once again, Luigi traverses up and down treacherous floors of the now-ominous hotel to save them.', 'Action', 'Nintendo', 'Nintendo Switch', '2019-10-31', 79.99, 'no', 79.99, '', 'product_pictures/LuigisMansion3/LuigisMansion3_01.jpg', 'product_pictures/LuigisMansion3/LuigisMansion3_02.jpg', 'product_pictures/LuigisMansion3/LuigisMansion3_03.jpg', 'https://www.youtube.com/watch?v=RSGgCfbYrg0'),
(19, 'Games', 'For Honor (PS 4)', 'Storm castles and slash your way through epic medieval-era battles in For Honor for PlayStation 4. Take control of a legendary Knight, Viking, or Samurai warrior and dive headlong into chaotic single-player campaign. Use the Art of Battle combat system to master sword fighting, then take your skills online in frenetic multiplayer matches.', 'Action', 'Ubisoft', 'PlayStation 4', '2017-02-13', 39.99, 'no', 39.99, '', 'product_pictures/ForHonor/ForHonor_01.jpg', 'product_pictures/ForHonor/ForHonor_02.jpg', 'product_pictures/ForHonor/ForHonor_03.jpg', 'https://www.youtube.com/watch?v=g6GGoTxvGzk'),
(20, 'Games', 'WWE 2K20 (Xbox One)', 'Step inside the ring with WWE 2K20 for Xbox One. Play as your favourite WWE superstars, legends, and Hall of Famers, plus NXT\'s best will also join the fun. WWE 2K20 features gameplay enhancements, steamlined controls, and exciting innovations.', 'Simulation', 'Other', 'Xbox One', '2019-10-22', 29.99, 'no', 29.99, '', 'product_pictures/WWE2K20/WWE2K20_01.jpg', 'product_pictures/WWE2K20/WWE2K20_02.jpg', 'product_pictures/WWE2K20/WWE2K20_03.jpg', 'https://www.youtube.com/watch?v=QwCV-Cuvko0'),
(21, 'Games', 'Super Smash Bros Ultimate (Switch)', 'Join the biggest brawl yet in Super Smash Bros Ultimate for Nintendo Switch. You\'ll find all your favourite fighters along with new characters like Simon Belmont and King K Rool. Battle on more than 100 stages, each one equipped with Battlefield and Final Destination versions. Choose from a variety of game modes to keep the challenges coming.', 'Action', 'Nintendo', 'Nintendo Switch', '2018-12-07', 79.99, 'no', 79.99, '', 'product_pictures/SSmashBrosUltimate/SSmashBrosUltimate_01.jpg', 'product_pictures/SSmashBrosUltimate/SSmashBrosUltimate_02.jpg', 'product_pictures/SSmashBrosUltimate/SSmashBrosUltimate_03.jpg', 'https://www.youtube.com/watch?v=EXnbMp1yr1k'),
(22, 'Games', 'Final Fantasy VII Remake (PS 4)', 'A reimagined version of the beloved roleplaying game, Final Fantasy VII Remake comes to PlayStation 4 in spectacular form. In the renowned RPG, the world has fallen under control of the shadowy Shinra Electric Power Company. In Midgar, the anti-Shinra organisation Avalanche steps up the resistance, as former Shinra soldier Cloud Strife joins the group.', 'RPG', 'Other', 'PlayStation 4', '2020-04-10', 79.99, 'no', 79.99, '', 'product_pictures/FFVIIRemake/FFVIIRemake_01.jpg', 'product_pictures/FFVIIRemake/FFVIIRemake_02.jpg', 'product_pictures/FFVIIRemake/FFVIIRemake_03.jpg', 'https://www.youtube.com/watch?v=ERgrFVhL-n4'),
(23, 'Games', 'Prey (Xbox One)', 'Prey Xbox One video game. While aboard Talos, a space station orbiting the moon, you need to use all you have to stay alive and away from the aliens. Rated mature 17 or more for blood, language, use of alcohol and violence. 1 player game.', 'Shooters', 'Other', 'Xbox One', '2017-05-05', 21.99, 'no', 21.99, '', 'product_pictures/Prey/Prey_01.jpg', 'product_pictures/Prey/Prey_02.jpg', 'product_pictures/Prey/Prey_03.jpg', 'https://www.youtube.com/watch?v=LNHZ9WAertc'),
(24, 'Games', 'Just Dance 2020 (Switch)', 'Get off the couch and on your feet with Just Dance 2020 for Nintendo Switch. It features 40 hot tracks like \"God Is a Woman\" by Ariana Grande and \"High Hopes\" by Panic! At The Disco. The entire family will be dancing for hours.', 'Dance', 'Ubisoft', 'Nintendo Switch', '2019-11-05', 29.99, 'no', 29.99, '', 'product_pictures/JustDance2020/JustDance2020_01.jpg', 'product_pictures/JustDance2020/JustDance2020_02.jpg', 'product_pictures/JustDance2020/JustDance2020_03.jpg', 'https://www.youtube.com/watch?v=9BrAT_o7yWA'),
(25, 'Games', 'Titanfall 2 (Xbox One)', 'Titan Fall 2 Xbox One video game. Product is a previously played video game, no damage to case, video game in very good condition. Rated mature 17 and plus for blood, gore, language, and violence. 1 player game.', 'Shooters', 'Other', 'Xbox One', '2016-10-28', 14.99, 'no', 14.99, '', 'product_pictures/Titanfall2/Titanfall2_01.jpg', 'product_pictures/Titanfall2/Titanfall2_02.jpg', 'product_pictures/Titanfall2/Titanfall2_03.jpg', 'https://www.youtube.com/watch?v=VqeMjHmL9eg'),
(26, 'Games', 'God of War (PS 4)', 'Join the god-slaying Spartan once again on a quest of survival in the God of War video game for PlayStation 4. Kratos puts his past behind and starts a new life of mentoring his son Atreus to prevail in the dangerous Norse world. Explore the extensive lands, master new abilities, and battle strong opponents in an action-packed gameplay.', 'Adventure', 'Other', 'PlayStation 4', '2018-04-20', 19.99, 'no', 19.99, '', 'product_pictures/GodOfWar/GodOfWar_01.jpg', 'product_pictures/GodOfWar/GodOfWar_02.jpg', 'product_pictures/GodOfWar/GodOfWar_03.jpg', 'https://www.youtube.com/watch?v=K0u_kAWLJOA'),
(27, 'Games', 'Pokemon Shield (Switch)', 'A new generation of Pokemon is coming to Nintendo Switch. Become a Pokemon Trainer and embark on a new journey in the new Galar region! Choose from one of three new partner Pokemon: Grookey, Scorbunny, or Sobble. Catch, battle, and trade new and familiar Pokemon while exploring new areas and uncovering an all-new story.', 'RPG', 'Other', 'Nintendo Switch', '2019-11-15', 79.99, 'no', 79.99, '', 'product_pictures/PokemonShield/PokemonShield_01.jpg', 'product_pictures/PokemonShield/PokemonShield_02.jpg', 'product_pictures/PokemonShield/PokemonShield_03.jpg', 'https://www.youtube.com/watch?v=rWwEeHB8K2Q'),
(28, 'Games', 'DisHonored: Death of The Outsider', 'DisHonored: Death of the Outsider Xbox One Video Game. Product has been previously played, is opened from the cellophane, with a small crack to back of the case. Video game is in very good condition with no scratches or cracks. Rated Mature 17 and plus with Blood and Gore included in the Video Game. Single Player game, with Xbox One X Enhanced.', 'Action', 'Bethesda', 'Xbox One', '2017-09-14', 14.99, 'no', 14.99, '', 'product_pictures/DishonoredDOTOutsider/DishonoredDOTOutsider_01.jpg', 'product_pictures/DishonoredDOTOutsider/DishonoredDOTOutsider_02.jpg', 'product_pictures/DishonoredDOTOutsider/DishonoredDOTOutsider_03.jpg', 'https://www.youtube.com/watch?v=XTq5pCTOI4o'),
(29, 'Games', 'Batman: Arkham Knight (PS 4)', 'In this explosive and long awaited Arkham series finale, the Scarecrow has returned to reunite super villains for battle. Batman: Arkham Knight for PlayStation 4 introduces Rocksteady\'s redesigned Batmobile and makes it drivable for the first time. Tear through streets and the skyline of Gotham City to participate in the final leg of this epic saga.', 'Action', 'Other', 'PlayStation 4', '2015-06-23', 24.99, 'no', 24.99, '', 'product_pictures/BatmanAK/BatmanAK_01.jpg', 'product_pictures/BatmanAK/BatmanAK_02.jpg', 'product_pictures/BatmanAK/BatmanAK_03.jpeg', 'https://www.youtube.com/watch?v=dxa34RatmSc'),
(30, 'Games', 'Pokemon Sword (Switch)', 'A new generation of Pokemon is coming to Nintendo Switch. Become a Pokemon Trainer and embark on a new journey in the new Galar region! Choose from one of three new partner Pokemon: Grookey, Scorbunny, or Sobble. Catch, battle, and trade new and familiar Pokemon while exploring new areas and uncovering an all-new story.', 'RPG', 'Other', 'Nintendo Switch', '2019-11-15', 79.99, 'no', 79.99, '', 'product_pictures/PokemonSword/PokemonSword_01.jpg', 'product_pictures/PokemonSword/PokemonSword_02.jpg', 'product_pictures/PokemonSword/PokemonSword_03.jpg', 'https://www.youtube.com/watch?v=grO7u1R866o'),
(31, 'Games', 'Disney Classic Games Aladdin and Lion King (Xbox One)', 'Two of the most adored Disney games of all time are making their long-awaited return to modern game consoles in Disney\'s classic animated film games: Aladin and The Lion King! This unforgettable set of Disney animation classics is full of new features, improvements and display options and new game modes, plus there are multiple versions of the games! Join Aladin and his sidekick, Abou, as they run across the market to their date with destiny. collecting precious stones during the journey.', 'Action', 'Other', 'Xbox One', '2019-10-29', 59.99, 'no', 59.99, '', 'product_pictures/AladdinLionKing/AladdinLionKing_01.jpg', 'product_pictures/AladdinLionKing/AladdinLionKing_02.jpg', 'product_pictures/AladdinLionKing/AladdinLionKing_03.jpeg', 'https://www.youtube.com/watch?v=HQDddZFwbSU'),
(32, 'Games', 'NFS Heat (PS 4)', 'Go head to head against a rogue police force in Need for Speed Heat for PlayStation 4. Battle your way into street racing\'s elite by competing in the Speedhunter Showdown where you\'ll earn money to customize and upgrade your high-performance cars. Then, head into the night to compete in illicit street races while you outwit the dishonest cops who want to take you down.', 'Racing', 'EA', 'PlayStation 4', '2019-11-08', 39.99, 'no', 39.99, '', 'product_pictures/NFSHeat/NFSHeat_01.jpg', 'product_pictures/NFSHeat/NFSHeat_02.jpg', 'product_pictures/NFSHeat/NFSHeat_03.jpg', 'https://www.youtube.com/watch?v=9ewiJJe_nYI'),
(33, 'Games', 'Overwatch Legendary Edition (Switch)', 'Choose your hero from a diverse cast of soldiers, scientists, adventurers, and oddities in Overwatch Legendary Edition for Nintendo Switch. Bend time, defy physics, and unleash an array of extraordinary powers and weapons. Engage your enemies in iconic locations from around the globe in the ultimate team-based shooter.', 'Action', 'Other', 'Nintendo Switch', '2019-10-15', 54.99, 'no', 54.99, '', 'product_pictures/OverwatchLE/OverwatchLE_01.jpg', 'product_pictures/OverwatchLE/OverwatchLE_02.jpg', 'product_pictures/OverwatchLE/OverwatchLE_03.jpg', 'https://www.youtube.com/watch?v=i39Pgu_36H0'),
(34, 'Games', 'Borderlands 3 (PS 4)', 'The original shooter-looter returns, packing bazillions of guns and an all-new mayhem-fueled adventure in Borderlands 3 for PlayStation 4. Blast through new worlds and enemies as one of four brand new treasure-seeking Vault Hunters, each with deep skill trees, abilities, and customization. Play solo or join with friends to take on insane enemies.', 'Shooters', 'Other', 'PlayStation 4', '2019-03-13', 29.99, 'no', 29.99, '', 'product_pictures/Borderlands3/Borderlands3_01.jpg', 'product_pictures/Borderlands3/Borderlands3_02.jpg', 'product_pictures/Borderlands3/Borderlands3_03.jpg', 'https://www.youtube.com/watch?v=d9Gu1PspA3Y'),
(35, 'Games', 'Cyberpunk 2077 (Xbox One)', 'Cyberpunk 2077 is a science fiction game loosely based on the role-playing game Cyberpunk 2020. Setting The game is set in the year 2077 in a fictional futuristic metropolis Night City in California. In the world of the game, there are developed cybernetic augmentations that enhance people\'s strength, agility, and memory. The city is governed by corporations. Many jobs are taken over by the robots, leaving a lot of people poor and homeless. ', 'Action', 'Other', 'Xbox One', '2020-09-17', 79.99, 'no', 79.99, '', 'product_pictures/Cyberpunk2077/Cyberpunk2077_01.jpg', 'product_pictures/Cyberpunk2077/Cyberpunk2077_02.jpg', 'product_pictures/Cyberpunk2077/Cyberpunk2077_03.jpg', 'https://www.youtube.com/watch?v=LembwKDo1Dk'),
(36, 'Games', 'SpongeBob SquarePants: Battle for Bikini Bottom (Switch)', 'Head down to your favourite pineapple under the sea in SpongeBob SquarePants: Battle for Bikini Bottom - Rehydrated for Nintendo Switch. Play as SpongeBob, Patrick, or Sandy and foil Plankton\'s evil plan to take over Bikini Bottoms with an army of robots. This remake of a cult classic has been beautifully updated with high-end visuals and modern gameplay.', 'Adventure', 'Other', 'Nintendo Switch', '2020-06-22', 29.99, 'no', 29.99, '', 'product_pictures/SpongebobBFBB/SpongebobBFBB_01.jpg', 'product_pictures/SpongebobBFBB/SpongebobBFBB_02.jpg', 'product_pictures/SpongebobBFBB/SpongebobBFBB_03.jpg', 'https://www.youtube.com/watch?v=LiNz025LLDg'),
(37, 'Games', 'Desperados III (Xbox One)', 'Journey to a modern-day Wild West in Desperados III for Xbox One. This real-time game puts you in charge of a group of unlikely heroes and heroines who are hunted by vicious bandits and corrupt lawmen. Travel through frontier towns and modern cities and use the skills of the Desperados to take out enemies, complete missions, and execute attacks.', 'Strategy', 'Other', 'Xbox One', '2020-06-16', 54.99, 'no', 54.99, '', 'product_pictures/Desperados3/Desperados3_01.jpg', 'product_pictures/Desperados3/Desperados3_02.jpg', 'product_pictures/Desperados3/Desperados3_03.jpg', 'https://www.youtube.com/watch?v=PeJ9G77rau4'),
(38, 'Games', 'Ghost of Tsushima Launch Edition (PS 4)', 'Defend Japan from a massive Mongol invasion in Ghost of Tsushima Launch Edition for PlayStation 4. In this open-world action-adventure epic, Jin Sakai wages an unconventional war to reclaim his home, Tsushima Island, from the Mongol empire in the late 13th century. To do this, he must break from samurai traditions and forge a new path to become the Ghost.', 'Adventure', 'Sony', 'PlayStation 4', '2020-07-17', 79.99, 'no', 79.99, '', 'product_pictures/Ghost of Tsushima/Ghost of Tsushima_01.jpg', 'product_pictures/Ghost of Tsushima/Ghost of Tsushima_02.jpg', 'product_pictures/Ghost of Tsushima/Ghost of Tsushima_03.jpg', 'https://www.youtube.com/watch?v=iqysmS4lxwQ'),
(39, 'Games', 'The Legend of Heroes: Trails of Cold Steel III (Switch)', 'Rean Schwarzer uncovers a dark plot that threatens his homeland. To face their enemies, he must prepare a new generation of heroes as an instructor at a new branch campus and guide them towards victory.', 'Role-Playing', 'Other', 'Nintendo Switch', '2020-06-30', 79.99, 'no', 79.99, '', 'product_pictures/LegendOfHeroesTOCS3/LegendOfHeroesTOCS3_01.jpeg', 'product_pictures/LegendOfHeroesTOCS3/LegendOfHeroesTOCS3_02.jpg', 'product_pictures/LegendOfHeroesTOCS3/LegendOfHeroesTOCS3_03.jpg', 'https://www.youtube.com/watch?v=CzapfUxhXa4'),
(40, 'Games', 'Marvel\'s Avengers (Xbox One)', 'Play as Earth\'s mightiest heroes in Marvel\'s Avengers. You can assume the role of Captain America, Thor, Hulk, Black Widow, and Iron Man in a new, original story. After a catastrophic accident the Avengers become outlaws, but have to come back together when the world faces a terrible threat.', 'Action', 'Other', 'Xbox One', '2020-09-04', 79.99, 'no', 79.99, '', 'product_pictures/MarvelsAvengers/MarvelsAvengers_01.jpg', 'product_pictures/MarvelsAvengers/MarvelsAvengers_02.jpg', 'product_pictures/MarvelsAvengers/MarvelsAvengers_03.jpg', 'https://www.youtube.com/watch?v=MNUjCu1q3rA'),
(41, 'Games', 'Tony Hawk\'s Pro Skater 1 + 2 (PS 4)', 'Grab your deck and griptape and get ready to drop in for a pair of the greatest skateboarding games ever made. Tony Hawk\'s Pro Skater 1 + 2 brings both games to one collection on PlayStation 4, remastered in HD and rebuilt from the ground up. Play as your favourite pro skaters, with all the classic levels and tricks in stunning new detail.', 'Sports', 'Other', 'PlayStation 4', '2020-09-04', 59.99, 'no', 59.99, '', 'product_pictures/TonyHawks12/TonyHawks12_01.jpg', 'product_pictures/TonyHawks12/TonyHawks12_02.jpg', 'product_pictures/TonyHawks12/TonyHawks12_03.jpg', 'https://www.youtube.com/watch?v=6-eYJRR5lyE'),
(42, 'Games', 'Paper Mario: The Origami King (Switch)', 'A fun, comedy-filled adventure unfolds with Paper Mario: The Origami King for Nintendo Switch. Mario and his new partner, Olivia, try to free Princess Peach\'s castle from the evil origami menace King Olly as they repair the damaged landscape, battle Folded Soldiers in strategic ring-based battles, and more. Get crafting as you solve puzzles and fold your way to victory.', 'Adventure', 'Nintendo', 'Nintendo Switch', '2020-07-17', 79.99, 'no', 79.99, '', 'product_pictures/PaperMarioOrigamiKing/PaperMarioOrigamiKing_01.jpg', 'product_pictures/PaperMarioOrigamiKing/PaperMarioOrigamiKing_02.jpg', 'product_pictures/PaperMarioOrigamiKing/PaperMarioOrigamiKing_03.jpg', 'https://www.youtube.com/watch?v=FX6DTLcWUdY');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `rating` float NOT NULL,
  `content` varchar(10000) NOT NULL,
  `creationTS` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `productId`, `userId`, `rating`, `content`, `creationTS`) VALUES
(3, 2, 6, 5, 'I\'m playing it again!', '2020-05-12 13:53:29'),
(6, 1, 7, 4, 'I love this game!', '2020-05-12 13:54:22'),
(7, 11, 7, 5, 'With amazing power come amazing responsibilities....', '2020-05-12 13:55:03'),
(8, 11, 19, 4, 'Awesome game, with great graphics', '2020-05-16 01:07:02'),
(15, 1, 1, 5, 'Great game! And it has a lot more after the ending...', '2020-05-17 18:54:38'),
(22, 1, 19, 2, 'gdfgdfgfdgdf', '2020-05-17 22:20:31'),
(23, 4, 19, 3, 'Love this game', '2020-05-18 00:23:41'),
(24, 6, 19, 4, 'Good graphics. Looks real', '2020-05-18 00:24:57'),
(26, 40, 18, 4, 'Nice game. ', '2020-05-18 06:12:09'),
(28, 7, 18, 4, 'Lots of fun', '2020-05-18 06:13:10'),
(29, 36, 18, 2, 'Bad quality', '2020-05-18 06:13:23'),
(30, 38, 18, 4, 'My favorite game', '2020-05-18 06:13:39'),
(31, 39, 18, 3, 'Bit boring', '2020-05-18 06:13:53'),
(32, 26, 5, 4, 'Best game ever', '2020-05-18 06:20:29'),
(33, 32, 5, 2, 'Boring but cute', '2020-05-18 06:22:29'),
(34, 34, 5, 5, 'Like it a lot', '2020-05-18 06:22:42'),
(35, 41, 5, 5, 'Super fun to play with friends', '2020-05-18 06:23:06'),
(36, 40, 6, 5, 'Great game. Lot of adventure', '2020-05-18 06:28:33'),
(37, 37, 6, 4, 'Great game with good graphics', '2020-05-18 06:28:57'),
(38, 42, 6, 5, 'Love this game', '2020-05-18 06:29:11'),
(39, 35, 6, 4, 'Great game', '2020-05-18 06:29:27'),
(40, 41, 24, 5, 'It brings great memories back!', '2020-05-18 15:33:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `phoneNo` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `apartment` varchar(20) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` enum('AB','MB','NB','NL','NT','NS','NU','ON','PE','QC','SK','YT') DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `picturePath` varchar(200) DEFAULT NULL,
  `secret` varchar(250) DEFAULT NULL,
  `creationDateTime` date NOT NULL,
  `identifier` varchar(250) DEFAULT NULL,
  `provider` enum('Facebook','Twitter','Google') DEFAULT NULL,
  `isAdmin` enum('false','true') NOT NULL,
  `stripeCustId` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `name`, `password`, `phoneNo`, `address`, `apartment`, `city`, `province`, `zipcode`, `picturePath`, `secret`, `creationDateTime`, `identifier`, `provider`, `isAdmin`, `stripeCustId`) VALUES
(1, 'jerry@mail.com', 'jerry1982mtl', 'Jerry Lenny', '02bb231511a09abb2a28a2be9d8e0fa0ff6349dc88809a26f1e2c5c64ec07681', '', '', '', '', 'QC', '', '/uploads/jerry1982mtl.png', NULL, '2020-05-04', NULL, NULL, 'false', NULL),
(5, 'maria@mail.com', 'maria1990', 'Maria da Silva', '02508a0eb6d1612890d88a93433b050d008d653076bdf9f341b243823fa77ca7', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/maria1990.png', NULL, '2020-05-04', NULL, NULL, 'false', 'cus_HIddMooMV35Sly'),
(6, 'terry@mail.com', 'terryGamer', 'Terry Lee', '60d82e060ad50da2c287d6680cd6111adcb56aededd9e1f4681328f30f1b88b7', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/terrygamer.png', NULL, '2020-05-05', NULL, NULL, 'false', NULL),
(7, 'tomas@mail.com', 'tomas123', 'Tomas Lee', '342cbbacba50e2130ee6d8a5d1e3d4e88ce729b10cb4561ac4967a01a7340dec', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/tomas123.png', NULL, '2020-05-05', NULL, NULL, 'false', NULL),
(13, 'daniel_genetica@yahoo.com.br', 'DanielGarcia-Santos', 'Daniel Garcia-Santos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://graph.facebook.com/v6.0/10158654071383939/picture?width=150&height=150', NULL, '0000-00-00', '10158654071383939', 'Facebook', 'false', NULL),
(16, NULL, 'Giovana Ourique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://pbs.twimg.com/profile_images/1259556555923623937/XXEBmN6o.jpg', NULL, '0000-00-00', '1258170170754899968', 'Twitter', 'false', NULL),
(18, 'giovana.ourique@hotmail.com', 'gihotmail', 'Giovana', 'd54d232490212d40cace6376e97d32a6a21455be46a4742ba406f44150a0bf9a', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/gihotmail.png', NULL, '2020-05-10', NULL, NULL, 'false', 'cus_HIdSmTg6UGENsM'),
(19, 'giovanaourique@gmail.com', 'giovana123', 'Giovana Ourique', NULL, '2514445555', '123 My Street', '01', 'Montreal', 'QC', 'H4H5H6', 'https://lh3.googleusercontent.com/a-/AOh14Gii111cSerbeJRDHrT2ccQlu7rk1qwxMNa8pzNYCA', NULL, '0000-00-00', '113644407270356878940', 'Google', 'false', 'cus_HId7Cp0IYhhvcL'),
(20, 'rachel@mail.com', 'rachelGreen', 'Rachel Green', '1d8c9e01964e5be081e0612300ed179e6df6b8add93022e85a75e16352702b9c', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/rachelgreen.png', NULL, '2020-05-11', NULL, NULL, 'false', NULL),
(21, 'john@mail.com', 'john1955', 'John Lee', '370420e335f7a4d2ead6879991641792d5bdcde2db5d4fae3d976c2133732cda', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/default.png', NULL, '2020-05-11', NULL, NULL, 'false', NULL),
(22, 'tina@mail.com', 'tinaGamer', 'Tina Smith', '05ad7397e81eb6b9f8f7388538b33a8159124cf9a2217e5cefc0ea48f9973805', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/tinagamer.png', NULL, '2020-05-11', NULL, NULL, 'false', NULL),
(23, 'tom@mail.com', 'tom_admin', 'Tom Smith', '40c828d39fb81edfe86580eaa0d3d048033d6edfe52a652250475746faac1910', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/tom_admin.png', NULL, '2020-05-14', NULL, NULL, 'false', 'cus_HHFaX7PfXcSV81'),
(24, 'mcjav2@gmail.com', 'mctom', 'Mc Tom', '40c828d39fb81edfe86580eaa0d3d048033d6edfe52a652250475746faac1910', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/default.png', NULL, '2020-05-12', NULL, NULL, 'true', NULL),
(25, 'shuixiutan@gmail.com', 'shuixiutan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://lh6.googleusercontent.com/-Rikl16BHFwY/AAAAAAAAAAI/AAAAAAAAAAA/c7y0QflHT7Q/photo.jpg', NULL, '0000-00-00', '112267508232360358906', 'Google', 'false', NULL),
(26, 'marilia@mail.com', 'marilia', 'Marilia da Silva', 'be5415c33a087c8e65245743676cfdee67053ab82170462aa7956ea23fe4918e', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/default.png', 'zC7uplROveLcizNXvKC4KgnJ2HpA2WNbDntXkJLIZnyPvqCLIJug2NBsqlJ8', '2020-05-14', NULL, NULL, 'false', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `productId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id`, `userId`, `productId`) VALUES
(8, 1, 6),
(30, 1, 41),
(27, 5, 26),
(26, 5, 32),
(25, 5, 34),
(24, 5, 41),
(29, 6, 37),
(28, 6, 40),
(21, 18, 7),
(20, 18, 35),
(23, 18, 36),
(19, 18, 38),
(22, 18, 39),
(18, 19, 5),
(16, 19, 9),
(15, 19, 11),
(17, 19, 37);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sessionId` (`sessionId`,`productId`),
  ADD KEY `productId` (`productId`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productId` (`productId`),
  ADD KEY `orderId` (`orderId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `passwordresets`
--
ALTER TABLE `passwordresets`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `productId_2` (`productId`,`userId`),
  ADD KEY `userId` (`userId`),
  ADD KEY `productId` (`productId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `identifier` (`identifier`,`provider`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `userId_2` (`userId`,`productId`),
  ADD KEY `userId` (`userId`),
  ADD KEY `productId` (`productId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cartitems`
--
ALTER TABLE `cartitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD CONSTRAINT `cartitems_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `products` (`id`);

--
-- Constraints for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD CONSTRAINT `orderitems_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `orderitems_ibfk_3` FOREIGN KEY (`orderId`) REFERENCES `orders` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

--
-- Constraints for table `passwordresets`
--
ALTER TABLE `passwordresets`
  ADD CONSTRAINT `passwordresets_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `reviews_ibfk_3` FOREIGN KEY (`productId`) REFERENCES `products` (`id`);

--
-- Constraints for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `wishlist_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `wishlist_ibfk_3` FOREIGN KEY (`productId`) REFERENCES `products` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
