-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: May 14, 2020 at 05:11 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gamestore`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `firstLastName` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `postalCode` varchar(10) NOT NULL,
  `phoneNo` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `orderDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `deliveryMethod` enum('PickUp','Delivery') NOT NULL,
  `deliveryCost` decimal(10,2) NOT NULL,
  `dueDate` datetime NOT NULL,
  `totalAmountBeforeTax` decimal(10,2) NOT NULL,
  `taxAmount` decimal(10,2) NOT NULL,
  `totalPaid` decimal(10,2) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `orderStatus` enum('Placed','Shipped','Delivered','Returned') NOT NULL,
  `userSCid` varchar(255) NOT NULL,
  `paymentStatus` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `userId`, `firstLastName`, `address`, `postalCode`, `phoneNo`, `email`, `orderDate`, `deliveryMethod`, `deliveryCost`, `dueDate`, `totalAmountBeforeTax`, `taxAmount`, `totalPaid`, `currency`, `orderStatus`, `userSCid`, `paymentStatus`) VALUES
('ch_1GiVqsKXTVMqOXOq22BExYso', 6, 'Tom Admin', '321 Main St', 'H2X 2E1', '4388201436', 'tom@mail.com', '2020-05-14 01:27:11', 'Delivery', '10.00', '2020-05-14 03:27:09', '129.99', '21.00', '160.99', 'cad', 'Placed', 'cus_HH3yNcYlRzz7jB', 'succeeded'),
('ch_1GiVwmKXTVMqOXOq2Tvm4wPp', 6, 'Tom Admin', '321 Main St', 'H2X 2E1', '4388201436', 'tom@mail.com', '2020-05-14 01:33:16', 'Delivery', '10.00', '2020-05-14 03:33:15', '154.99', '24.75', '189.74', 'cad', 'Placed', 'cus_HH44jwukbHRvKF', 'succeeded'),
('ch_1GiWHJKXTVMqOXOqDt9MY9vJ', 6, 'Tom Admin', '321 Mi Street', 'H2X 2E1', '4388201436', 'tom@mail.com', '2020-05-14 01:54:29', 'Delivery', '10.00', '2020-05-14 03:54:27', '194.98', '30.75', '235.73', 'cad', 'Placed', 'cus_HH4Qc6lGrkn7DP', 'succeeded'),
('ch_1GiX1BKXTVMqOXOqF0Fe8DBq', 6, 'TOm Smith', '3450 Durocher', 'H2X 2E1', '4388201436', 'tom@mail.com', '2020-05-14 02:41:53', 'Delivery', '10.00', '2020-05-14 04:41:52', '179.99', '28.50', '218.49', 'cad', 'Placed', 'cus_HH5BV3edU6AlKX', 'succeeded'),
('ch_1GiXF7KXTVMqOXOqaqsWufOS', 6, 'Tom Smith-Smith', '321 Main St', 'H2X 2E1', '4388201436', 'tom@mail.com', '2020-05-14 02:56:18', 'Delivery', '10.00', '2020-05-14 04:56:16', '79.99', '13.50', '103.49', 'cad', 'Placed', 'cus_HH5PDQQOVT2Mkx', 'succeeded'),
('ch_1GiXJrKXTVMqOXOqjnC0Cq77', 6, 'Tom Smith-Smith', '3450 Durocher', 'H2X 2E1', '4388201436', 'tom@mail.com', '2020-05-14 03:01:11', 'Delivery', '10.00', '2020-05-14 05:01:10', '139.99', '22.50', '172.49', 'cad', 'Placed', 'cus_HH5UHhdZrMK0ut', 'succeeded');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
