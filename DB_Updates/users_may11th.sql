-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 11, 2020 at 08:34 AM
-- Server version: 10.3.22-MariaDB-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cp4966_gamestore`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `phoneNo` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `apartment` varchar(20) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` enum('AB','MB','NB','NL','NT','NS','NU','ON','PE','QC','SK','YT') DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `picturePath` varchar(200) DEFAULT NULL,
  `secret` varchar(250) DEFAULT NULL,
  `creationDateTime` date NOT NULL,
  `identifier` varchar(250) DEFAULT NULL,
  `provider` enum('Facebook','Twitter','Google') DEFAULT NULL,
  `isAdmin` enum('false','true') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `name`, `password`, `phoneNo`, `address`, `apartment`, `city`, `province`, `zipcode`, `picturePath`, `secret`, `creationDateTime`, `identifier`, `provider`, `isAdmin`) VALUES
(1, 'jerry@mail.com', 'jerry1982', 'Jerry Lee', 'Jerry123', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/default.png', NULL, '2020-05-04', NULL, NULL, 'false'),
(5, 'maria@mail.com', 'maria1990', 'Maria da Silva', 'Maria123', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/maria1990.jpg', NULL, '2020-05-04', NULL, NULL, 'false'),
(6, 'terry@mail.com', 'terryGamer', 'Terry Lee', 'Terry123', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/default.png', NULL, '2020-05-05', NULL, NULL, 'false'),
(7, 'tomas@mail.com', 'tomas123', 'Tomas Lee', 'Tomas123', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/tomas123.jpg', NULL, '2020-05-05', NULL, NULL, 'false'),
(13, 'daniel_genetica@yahoo.com.br', 'DanielGarcia-Santos', 'Daniel Garcia-Santos', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://graph.facebook.com/v6.0/10158654071383939/picture?width=150&height=150', NULL, '0000-00-00', '10158654071383939', 'Facebook', 'false'),
(16, NULL, 'Giovana Ourique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://pbs.twimg.com/profile_images/1259556555923623937/XXEBmN6o.jpg', NULL, '0000-00-00', '1258170170754899968', 'Twitter', 'false'),
(18, 'giovana.ourique@hotmail.com', 'gihotmail', 'Giovana', 'Giovana123', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/default.png', NULL, '2020-05-10', NULL, NULL, 'false'),
(19, 'giovanaourique@gmail.com', 'giovanaourique', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'https://lh3.googleusercontent.com/a-/AOh14Gii111cSerbeJRDHrT2ccQlu7rk1qwxMNa8pzNYCA', NULL, '0000-00-00', '113644407270356878940', 'Google', 'false'),
(20, 'rachel@mail.com', 'rachelGreen', 'Rachel Green', 'Rachel123', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/rachelgreen.png', NULL, '2020-05-11', NULL, NULL, 'false'),
(21, 'john@mail.com', 'john1955', 'John Lee', 'John123', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/default.png', NULL, '2020-05-11', NULL, NULL, 'false'),
(22, 'tina@mail.com', 'tinaGamer', 'Tina Smith', 'Tina123', NULL, NULL, NULL, NULL, NULL, NULL, '/uploads/tinagamer.jpg', NULL, '2020-05-11', NULL, NULL, 'false');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
