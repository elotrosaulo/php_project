-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: May 07, 2020 at 11:20 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gamestore`
--

-- --------------------------------------------------------

--
-- Table structure for table `cartitems`
--

CREATE TABLE `cartitems` (
  `id` int(11) NOT NULL,
  `sessionId` varchar(100) NOT NULL,
  `addedTS` timestamp NOT NULL DEFAULT current_timestamp(),
  `productId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `orderheaders`
--

CREATE TABLE `orderheaders` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `firstLastName` varchar(100) NOT NULL,
  `address` varchar(250) NOT NULL,
  `postalCode` varchar(10) NOT NULL,
  `phoneNo` varchar(20) NOT NULL,
  `orderDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `deliveryMethod` enum('Pickup','Delivery') NOT NULL,
  `deliveryCost` decimal(10,2) NOT NULL,
  `dueDate` datetime NOT NULL,
  `totalAmountBeforeTax` decimal(10,2) NOT NULL,
  `taxAmount` decimal(10,2) NOT NULL,
  `totalPaid` decimal(10,2) NOT NULL,
  `status` enum('Placed','Shipped','Delivered','Returned') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ordersitems`
--

CREATE TABLE `ordersitems` (
  `id` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `unitPrice` decimal(10,0) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `productDescription` varchar(10000) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `contactInformation` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category` enum('Games','Consoles','Acessories') NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `game_genre` enum('Action','Adventure','Arcade','Dance','Fighting','Horror','Party','Puzzle','Racing','Role-Playing','RPG','Sci-Fi','Shooters','Simulation','Sports','Strategy') DEFAULT NULL,
  `developer` enum('Nintendo','Microsoft','Sony','Bethesda','Ubisoft','EA','Other','Atari','NaughtyDog') NOT NULL,
  `platform` enum('Nintendo DS','Nintendo 3DS & 2DS','Nintendo Switch','PC','PlayStation 3','PlayStation 4','PlayStation Vita','PlayStation VR','Xbox 360','Xbox One','Wii','Wii U') NOT NULL,
  `releaseDate` date NOT NULL,
  `unitPrice` decimal(10,2) NOT NULL,
  `onSale` enum('yes','no') NOT NULL,
  `newPrice` decimal(10,2) DEFAULT NULL,
  `video` varchar(250) DEFAULT NULL,
  `picture1` varchar(250) DEFAULT NULL,
  `picture2` varchar(250) DEFAULT NULL,
  `picture3` varchar(250) DEFAULT NULL,
  `url` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category`, `name`, `description`, `game_genre`, `developer`, `platform`, `releaseDate`, `unitPrice`, `onSale`, `newPrice`, `video`, `picture1`, `picture2`, `picture3`, `url`) VALUES
(1, 'Games', 'Super Mario Odyssey (Switch)', 'Join Mario on a massive, globe-trotting 3D adventure and use his incredible new abilities to collect Moons so you can power up your airship, the Odyssey, and rescue Princess Peach from Bowser\'s wedding plans! This sandbox-style 3D Mario adventure—the first since 1996\'s beloved Super Mario 64 and 2002\'s Nintendo GameCube classic Super Mario Sunshine—is packed with secrets and surprises, and with Mario\'s new moves like cap throw, cap jump, and capture, you\'ll have fun and exciting gameplay experiences unlike anything you\'ve enjoyed in a Mario game before. Get ready to be whisked away to strange and amazing places far from the Mushroom Kingdom!', 'Adventure', 'Nintendo', 'Nintendo Switch', '2017-10-27', '79.99', 'no', '79.99', NULL, '/product_pictures/MarioOdyssey/MarioOdyssey_01.jpg', '/product_pictures/MarioOdyssey/MarioOdyssey_02.jpg', '/product_pictures/MarioOdyssey/MarioOdyssey_03.jpg', 'https://www.youtube.com/watch?v=wGQHQc_3ycE'),
(2, 'Games', 'The Legend of Zelda: Breath of the Wild (Switch)', 'Forget everything you know about The Legend of Zelda games. Step into a world of discovery, exploration, and adventure in The Legend of Zelda: Breath of the Wild, a boundary-breaking new game in the acclaimed series. Travel across vast fields, through forests, and to mountain peaks as you discover what has become of the kingdom of Hyrule in this stunning Open-Air Adventure. Now on Nintendo Switch, your journey is freer and more open than ever. Take your system anywhere, and adventure as Link any way you like.', 'RPG', 'Nintendo', 'Nintendo Switch', '2017-03-03', '79.99', 'no', '79.99', NULL, '/product_pictures/ZeldaBOTW/ZeldaBOTW_01.jpg', '/product_pictures/ZeldaBOTW/ZeldaBOTW_02.jpg', '/product_pictures/ZeldaBOTW/ZeldaBOTW_03.jpg', 'https://www.youtube.com/watch?v=zw47_q9wbBE'),
(3, 'Games', 'Animal Crossing: New Horizons (Switch)', 'Escape to a deserted island and create your own paradise as you explore, create, and customize in the Animal Crossing: New Horizons game. Your island getaway has a wealth of natural resources that can be used to craft everything from tools to creature comforts. You can hunt down insects at the crack of dawn, decorate your paradise throughout the day, or enjoy sunset on the beach while fishing in the ocean. The time of day and season match real life, so each day on your island is a chance to check in and find new surprises all year round.\r\n\r\nShow off your island utopia to family and friends—or pack your bags and visit theirs. Whether playing online* or with others beside you**, island living is even better when you can share it. Even without hopping on a flight, you’ll meet a cast of charming animal residents bursting with personality. Friendly faces like Tom Nook and Isabelle will lend their services and happily help you grow your budding community. Escape to your island getaway—however, whenever, and wherever you want.', 'Simulation', 'Nintendo', 'Nintendo Switch', '2020-03-20', '79.99', 'no', '79.99', NULL, '/product_pictures/AnimalCrossing/AnimalCrossing_01.jpg', '/product_pictures/AnimalCrossing/AnimalCrossing_02.jpg', '/product_pictures/AnimalCrossing/AnimalCrossing_03.jpg', 'https://www.youtube.com/watch?v=sRWjpjNVOCM'),
(4, 'Games', 'Doom Eternal (PS4)', 'Doom Eternal for Playstation 4 is the direct sequel to the award winning original game DOOM. As the Doom slayer, you\'ll engage in first person combat as you seek your vengeance against the forces of evil. Slay demons with powerful new weapons and abilities in this pulse pounding thriller which allows you to fight across multiple dimensions.  ', 'Shooters', 'Bethesda', 'PlayStation 4', '2020-03-20', '59.99', 'no', '59.99', NULL, '/product_pictures/DOOMEternal/DOOMEternal_01.jpg', '/product_pictures/DOOMEternal/DOOMEternal_02.jpg', '/product_pictures/DOOMEternal/DOOMEternal_03.jpg', 'https://www.youtube.com/watch?v=qgvV4GE8vVA'),
(5, 'Games', 'The Last of Us Part II (PS4)', ' Five years after their dangerous journey across the post-pandemic United States, Ellie and Joel have settled down in Jackson, Wyoming. Living amongst a thriving community of survivors has allowed them peace and stability, despite the constant threat of the infected and other, more desperate survivors.\r\n\r\nWhen a violent event disrupts that peace, Ellie embarks on a relentless journey to carry out justice and find closure. As she hunts those responsible one by one, she is confronted with the devastating physical and emotional repercussions of her actions. ', 'Action', 'NaughtyDog', 'PlayStation 4', '2020-05-29', '79.99', 'no', '79.99', '', '/product_pictures/LastOfUsII/LastOfUsII_01.jpeg', '/product_pictures/LastOfUsII/LastOfUsII_02.jpg', '/product_pictures/LastOfUsII/LastOfUsII_03.jpg', 'https://www.youtube.com/watch?v=X0VubwgS2Y4'),
(6, 'Games', 'FIFA 20 (XBox One)', 'EA SPORTS FIFA 20 for the Xbox One brings two sides of The World’s Game to life -- the prestige of the professional stage and an all-new authentic street football experience in EA SPORTS VOLTA. FOOTBALL INTELLIGENCE unlocks a platform for gameplay realism, FIFA Ultimate TeamTM offers more ways to build your dream squad, and EA SPORTS VOLTA returns the game to the street.', 'Sports', 'EA', 'Xbox One', '2019-09-24', '54.99', 'yes', '24.99', NULL, '/product_pictures/FIFA20//FIFA20_01.jpg', '/product_pictures/FIFA20//FIFA20_02.jpg', '/product_pictures/FIFA20//FIFA20_03.jpg', 'https://www.youtube.com/watch?v=vgQNOIhRsV4'),
(7, 'Games', 'Minecraft Dungeons (PC)', 'Minecraft Dungeons. An all-new action-adventure game, inspired by classic dungeon crawlers and set in the Minecraft universe! Brave the dungeons alone, or team up with friends! Up to four players can battle together online or in couch co-op. \r\n\r\nUse melee swings, hang back with ranged attacks, or tank your way through shielded by heavy armour! Personalize your character and unlock unique items and weapon enchantments for devastating special attacks. Explore the action-packed, treasure-stuffed levels – all in an epic quest to save the villagers and take down the evil Arch-Illager!', 'Adventure', 'Other', 'PC', '2020-05-26', '39.99', 'no', '39.99', NULL, '/product_pictures/MinecraftDungeons/MinecraftDungeons_01.jpg', '/product_pictures/MinecraftDungeons/MinecraftDungeons_02.png', '/product_pictures/MinecraftDungeons/MinecraftDungeons_03.jpg', 'https://www.youtube.com/watch?v=mJczpIdONjs'),
(8, 'Consoles', 'Nintendo Switch', 'Get the gaming system that lets you play the games you want, wherever you are, however you like.\r\n\r\nIncludes the Nintendo Switch console and Nintendo Switch dock in black, with contrasting left and right Joy‑Con controllers—one red, one blue. Also includes all the extras you need to get started.\r\n\r\nModel number: HAC-001(-01) (product serial number begins with “XKW”)\r\n\r\nThis model includes battery life of approximately 4.5 - 9 hours*.\r\n\r\n*The battery life will depend on the games you play. For instance, the battery will last approximately 5.5 hours for The Legend of Zelda: Breath of the Wild.', NULL, 'Nintendo', 'Nintendo Switch', '2017-03-03', '399.99', 'no', '399.99', NULL, '/product_pictures/NintendoSwitch/NintendoSwitch_01.jpg', '/product_pictures/NintendoSwitch/NintendoSwitch_02.jpg', '/product_pictures/NintendoSwitch/NintendoSwitch_03.jpg', 'https://www.youtube.com/watch?v=f5uik5fgIaI'),
(9, 'Games', 'Moving Out (Xbox One)', 'Moving Out is a ridiculous physics-based moving simulator that brings new meaning to \"couch co-op\"! Are you ready for an exciting career in furniture? As a newly certified Furniture Arrangement & Relocation Technician, youâ€™ll take on moving jobs all across the busy town of Packmore.', 'Simulation', 'Other', 'Xbox One', '2020-04-28', '25.00', 'no', '25.00', '', '', '', '', 'https://www.youtube.com/watch?v=7GmIvduEzP8');

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `rating` float NOT NULL,
  `content` varchar(10000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(250) NOT NULL,
  `username` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `password` varchar(100) DEFAULT NULL,
  `phoneNo` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `apartment` varchar(20) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `province` enum('AB','MB','NB','NL','NT','NS','NU','ON','PE','QC','SK','YT') DEFAULT NULL,
  `zipcode` varchar(20) DEFAULT NULL,
  `picturePath` varchar(200) DEFAULT NULL,
  `secret` varchar(250) DEFAULT NULL,
  `creationDateTime` date NOT NULL,
  `providerId` varchar(250) DEFAULT NULL,
  `provider` enum('Facebook','Twitter','Google') DEFAULT NULL,
  `isAdmin` enum('false','true') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `name`, `password`, `phoneNo`, `address`, `apartment`, `city`, `province`, `zipcode`, `picturePath`, `secret`, `creationDateTime`, `providerId`, `provider`, `isAdmin`) VALUES
(1, 'jerry@mail.com', 'jerry1982', 'Jerry Lee', 'Jerry123', NULL, NULL, NULL, NULL, NULL, NULL, 'default.png', NULL, '2020-05-04', NULL, NULL, 'false'),
(5, 'maria@mail.com', 'maria1990', 'Maria da Silva', 'Maria123', NULL, NULL, NULL, NULL, NULL, NULL, 'maria1990.jpg', NULL, '2020-05-04', NULL, NULL, 'false'),
(6, 'terry@mail.com', 'terryGamer', 'Terry Lee', 'Terry123', NULL, NULL, NULL, NULL, NULL, NULL, 'default.png', NULL, '2020-05-05', NULL, NULL, 'false'),
(7, 'tomas@mail.com', 'tomas123', 'Tomas Lee', 'Tomas123', NULL, NULL, NULL, NULL, NULL, NULL, 'tomas123.jpg', NULL, '2020-05-05', NULL, NULL, 'false'),
(10, 'giovanaourique@gmail.com', 'giovana', 'giovana', 'Giovana123', NULL, NULL, NULL, NULL, NULL, NULL, 'default.png', NULL, '2020-05-05', NULL, NULL, 'false');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `productId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sessionId` (`sessionId`,`productId`),
  ADD KEY `productId` (`productId`);

--
-- Indexes for table `orderheaders`
--
ALTER TABLE `orderheaders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `ordersitems`
--
ALTER TABLE `ordersitems`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `orderId` (`orderId`,`productId`),
  ADD KEY `productId` (`productId`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `productId` (`productId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `productId` (`productId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cartitems`
--
ALTER TABLE `cartitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orderheaders`
--
ALTER TABLE `orderheaders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ordersitems`
--
ALTER TABLE `ordersitems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cartitems`
--
ALTER TABLE `cartitems`
  ADD CONSTRAINT `cartitems_ibfk_1` FOREIGN KEY (`productId`) REFERENCES `products` (`id`);

--
-- Constraints for table `orderheaders`
--
ALTER TABLE `orderheaders`
  ADD CONSTRAINT `orderheaders_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

--
-- Constraints for table `ordersitems`
--
ALTER TABLE `ordersitems`
  ADD CONSTRAINT `ordersitems_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `orderheaders` (`id`),
  ADD CONSTRAINT `ordersitems_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `products` (`id`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `reviews_ibfk_3` FOREIGN KEY (`productId`) REFERENCES `products` (`id`);

--
-- Constraints for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `wishlist_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `wishlist_ibfk_3` FOREIGN KEY (`productId`) REFERENCES `products` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
