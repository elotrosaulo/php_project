<?php

use DI\Container;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Hybridauth\Exception\Exception;
use Hybridauth\Hybridauth;
use Hybridauth\HttpClient;
use Hybridauth\Storage\Session;

require_once "setup.php";
require 'vendor/autoload.php';

//******===============  ADMIN ONLY  ================******//
//******=============================================******//

// Add / Edit Product page ============================================================================================
// STATE 1: first show of the form
$app->map(['GET', 'POST'], '/product/add[/{productId:[0-9]+}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    global $log;
    $postVars = $request->getParsedBody();
    $errorsArray = array();
    $newId = '';

    if (!isset($_SESSION['user']) || $_SESSION['user']['isAdmin'] != 'true') {
        return $view->render($response, 'error_forbidden.html.twig');
    }

    if(isset($args['productId'])){
        $productId = $args['productId'];

        $postVars = DB::queryFirstRow("SELECT * FROM products WHERE id = %s", $productId);



            // DB::update('products', ['category' => $category, 'name' => $name, 'description' => $description, 'game_genre' => $game_genre, 'developer' => $developer, 
            // 'platform' => $platform, 'releaseDate' => $releaseDate, 'unitPrice' => $unitPrice, 'onSale' => $onSale, 'newPrice' => $newPrice, 'video' => $video1path, 
            // 'picture1' => $picture1path, 'picture2' => $picture2path, 'picture3' => $picture3path, 'url' => $url ], 'id=%s', $productId);
    }

    $db = 'gamestore';          // for local version
    //$db = 'cp4966_gamestore';   //for online version
    $tableName = 'products';
    $category ='category';
    $genre ='game_genre';
    $developer ='developer';
    $platform ='platform';
    $categoriesList = DB::queryFirstRow("SELECT COLUMN_TYPE AS categories FROM INFORMATION_SCHEMA.COLUMNS 
                         WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s AND COLUMN_NAME = %s", $db, $tableName, $category)['categories']; 
    preg_match("/^enum\(\'(.*)\'\)$/", $categoriesList, $matches);
    $categoriesList = explode("','", $matches[1]);

    $genresList = DB::queryFirstRow("SELECT COLUMN_TYPE AS categories FROM INFORMATION_SCHEMA.COLUMNS 
                         WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s AND COLUMN_NAME = %s", $db, $tableName, $genre)['categories']; 
    preg_match("/^enum\(\'(.*)\'\)$/", $genresList, $matches);
    $genresList = explode("','", $matches[1]);

    $developersList = DB::queryFirstRow("SELECT COLUMN_TYPE AS categories FROM INFORMATION_SCHEMA.COLUMNS 
                         WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s AND COLUMN_NAME = %s", $db, $tableName, $developer)['categories']; 
    preg_match("/^enum\(\'(.*)\'\)$/", $developersList, $matches);
    $developersList = explode("','", $matches[1]);

    $platformsList = DB::queryFirstRow("SELECT COLUMN_TYPE AS categories FROM INFORMATION_SCHEMA.COLUMNS 
                         WHERE TABLE_SCHEMA = %s AND TABLE_NAME = %s AND COLUMN_NAME = %s", $db, $tableName, $platform)['categories']; 
    preg_match("/^enum\(\'(.*)\'\)$/", $platformsList, $matches);
    $platformsList = explode("','", $matches[1]);    


    // STATE 2&3: receiving submission
    if ($request->getMethod() == 'POST') {
          
        // if (!isset($_SESSION['user']) || $_SESSION['user']['isAdmin'] != 'true') {
        //     return $view->render($response, '/forbidden');
        // }

        // extract submitted values
        $postVars = $request->getParsedBody();

        $category = $postVars['category'];
        $name = $postVars['name'];
        $description = $postVars['description'];
        $game_genre = $postVars['game_genre'];
        $developer = $postVars['developer'];
        $platform = $postVars['platform'];
        $releaseDate = $postVars['releaseDate'];
        $unitPrice = $postVars['unitPrice'];
        $folderName = $postVars['folderName'];
        $newPrice = $postVars['newPrice'];
        $video = "";
        $url = $postVars['url'];

        // check validity
        $errorsArray = array();
        // add Category validation

        // Name validation 
        if (preg_match('/^[a-zA-Z0-9!@#$%&*().:\-\+ \'\"]{1,100}$/', $name) != 1) {
        array_push($errorsArray, "Product name must be 1-100 characters long, and only consist of letters, digits, and underscores");
        $postVars['name'] = $name;
        } 

        // Description validation 
        if (preg_match('/^[a-zA-Z0-9!@#$%&*()\-\+,.: \'\"]{1,10000}$/', $description) != 1) {
        array_push($errorsArray, "Description must be 1-10000 characters long, and only consist of uppercase/lowercase letters, numbers and !@#$%&*(),.");
        $postVars['description'] = $description;
        }

        // add Game Genre validation
        // add Developer validation
        // add Platform validation

        // Release Date validation 
        if (!date_create_from_format('Y-m-d', $releaseDate)) {
            array_push($errorsArray, "Release date has invalid format");
        }
        $releaseDate = strtotime($releaseDate);
        if ($releaseDate < strtotime('1970-01-01') || $releaseDate > strtotime('2100-01-01')) {
            array_push($errorsArray, "Release date must be in the 1970 to 2099 range.");
        } else {
            $releaseDate = $postVars['releaseDate'];    // To come back to a date standard format (if not it will continue in seconds from 1970)
        }

        // Unit Price validation
        if ($unitPrice == "" || $unitPrice < 0 || $unitPrice > 1000) {
            array_push($errorsArray, "Unit Price must be a number in the 1-1000 dollars range");
            $postVars['name'] = "";
        }

        // On Sale validation
        if(!isset($postVars['onSale'])){
            $onSale = 'no';
        } else{
            if($postVars['onSale'] == 'on'){
                $onSale = 'yes';
            } else if ($postVars['onSale'] == ''){
                $onSale = 'no';
            } else {
                global $log;
                $log->error("Invalid 'onSale' value for inserting in the database");
                return $view->render($response, '/error_internal');
            }
        }

        // New Price validation
        if ($newPrice == "" || $newPrice < 0 || $newPrice > 1000) {
            array_push($errorsArray, "Unit Price must be a number in the 1-1000 dollars range");
            $postVars['name'] = "";
        }

        // Folder Name validation 
        if (preg_match('/^[a-zA-Z0-9_ ]{1,100}$/', $folderName) != 1) {
            array_push($errorsArray, "Folder name must be 1-100 characters long, and only consist of letters, digits, spaces and underscores");
            $postVars['folderName'] = "";
        } 
        // Create folder for media files if it doesn't exist:
        if (!file_exists('product_pictures/' . $folderName)) {
            mkdir('product_pictures/' . $folderName, 0777, true);
        }

        // Create folder for video files inside the just created folder, if it doesn't exist:
        if (!file_exists('product_pictures/' . $folderName . '/video')) {
            mkdir('product_pictures/' . $folderName . '/video', 0777, true);
        }

        // Picture 1 validation
        $productImage1 = $_FILES['picture1'];
        if ($productImage1['error'] != 0) {
            array_push($errorsArray, "File submission failed, make sure you've selected an image (1)");
        } else {
            $data = getimagesize($productImage1['tmp_name']);
            if ($data == FALSE) {
                array_push($errorsArray, "File submission failed, make sure you've selected an image (2)");
            } else {
                if (!in_array($data['mime'], array('image/jpg', 'image/jpeg', 'image/gif', 'image/png'))) {
                    array_push($errorsArray, "File submission failed, make sure you've selected an image (jpg, jpeg, png, gif)(3)");
                } else {
                    $productImage1['name'] = strtolower($productImage1['name']);
                // $productImage1 = strip_tags($name); //Sanitize
                    if (!preg_match('/.\.(jpg|jpeg|png|gif)$/', $productImage1['name'])) {
                        array_push($errorsArray, "File submission failed, make sure you've selected an image (4)");
                    }
                    $info = pathinfo($productImage1['name']);
                    $productImage1['name'] = preg_replace('[^a-zA-Z0-9_\.-]', '_', $folderName . "_01");   //Replace image name by game name (folder)
                    if (file_exists('product_pictures/' . $folderName . '/' . $productImage1['name'])) {    // If file exists, rename it
                        $num = 1;
                        while (file_exists('product_pictures/' . $folderName . '/' . $productImage1['name'] . "_$num." . $info['extension'])) {
                            $num++;
                        }
                        $productImage1['name'] = $info['filename'] . "_$num." . $info['extension'];
                    } else{
                        $productImage1['name'] = $productImage1['name'] . "." . $info['extension'];
                    }
                }
            }
        }
        // TODO: check dimensions $size = $productImage1->getSize(); 

        // Picture 2 validation
        $productImage2 = $_FILES['picture2'];
        if ($productImage2['error'] != 0) {
            array_push($errorsArray, "File submission failed, make sure you've selected an image (1)");
        } else {
            $data = getimagesize($productImage2['tmp_name']);
            if ($data == FALSE) {
                array_push($errorsArray, "File submission failed, make sure you've selected an image (2)");
            } else {
                if (!in_array($data['mime'], array('image/jpg', 'image/jpeg', 'image/gif', 'image/png'))) {
                    array_push($errorsArray, "File submission failed, make sure you've selected an image (jpg, jpeg, png, gif)(3)");
                } else {
                    $productImage2['name'] = strtolower($productImage2['name']);
                // $productImage1 = strip_tags($name); //Sanitize
                    if (!preg_match('/.\.(jpg|jpeg|png|gif)$/', $productImage2['name'])) {
                        array_push($errorsArray, "File submission failed, make sure you've selected an image (4)");
                    }
                    $info = pathinfo($productImage2['name']);
                    $productImage2['name'] = preg_replace('[^a-zA-Z0-9_\.-]', '_', $folderName . "_02");    //Replace image name by game name (folder)
                    if (file_exists('product_pictures/' . $folderName . '/' . $productImage2['name'])) {    // If file exists, rename it
                        $num = 1;
                        while (file_exists('product_pictures/' . $folderName . '/' . $productImage2['name'] . "_$num." . $info['extension'])) {
                            $num++;
                        }
                        $productImage2['name'] = $info['filename'] . "_$num." . $info['extension'];
                    } else{
                        $productImage2['name'] = $productImage2['name'] . "." . $info['extension'];
                    }
                }
            }
        }
        // TODO: check dimensions $size = $productImage2->getSize(); 

        // Picture 3 validation
        $productImage3 = $_FILES['picture3'];
        if ($productImage3['error'] != 0) {
            array_push($errorsArray, "File submission failed, make sure you've selected an image (1)");
        } else {
            $data = getimagesize($productImage3['tmp_name']);
            if ($data == FALSE) {
                array_push($errorsArray, "File submission failed, make sure you've selected an image (2)");
            } else {
                if (!in_array($data['mime'], array('image/jpg', 'image/jpeg', 'image/gif', 'image/png'))) {
                    array_push($errorsArray, "File submission failed, make sure you've selected an image (jpg, jpeg, png, gif)(3)");
                } else {
                    $productImage3['name'] = strtolower($productImage3['name']);
                // $productImage1 = strip_tags($name); //Sanitize
                    if (!preg_match('/.\.(jpg|jpeg|png|gif)$/', $productImage3['name'])) {
                        array_push($errorsArray, "File submission failed, make sure you've selected an image (4)");
                    }
                    $info = pathinfo($productImage3['name']);
                    $productImage3['name'] = preg_replace('[^a-zA-Z0-9_\.-]', '_', $folderName . "_03");   // Replace image name by game name (folder)
                    if (file_exists('product_pictures/' . $folderName . '/' . $productImage3['name'])) {   // If file exists, rename it
                        $num = 1;
                        while (file_exists('product_pictures/' . $folderName . '/' . $productImage3['name'] . "_$num." . $info['extension'])) {
                            $num++;
                        }
                        $productImage3['name'] = $info['filename'] . "_$num." . $info['extension'];
                    } else{
                        $productImage3['name'] = $productImage3['name'] . "." . $info['extension'];
                    }
                }
            }
        }
        // TODO: check dimensions $size = $productImage3->getSize(); 

        // // Video validation
        $productVideo1 = $_FILES['video1'];
        if ($productVideo1['error'] != 0) {
            array_push($errorsArray, "File submission failed, make sure you've selected a video (1)");
        } else {
            $data = filesize($productVideo1['tmp_name']);
            if ($data == FALSE) {
                array_push($errorsArray, "File submission failed, make sure you've selected a video (2)");
            } else {
                if (!in_array($productVideo1['type'], array('video/mp4', 'video/wma'. 'video/mov'))) {
                    array_push($errorsArray, "File submission failed, make sure you've selected a video (mp4, wma, mov)(3)");
                } else {              
                    if (!preg_match('/.\.(mp4|wma|mov)$/', $productVideo1['name'])) {
                        array_push($errorsArray, "File submission failed, make sure you've selected a video (4)");
                    }
                    $info = pathinfo($productVideo1['name']);
                    $productVideo1['name'] = preg_replace('[^a-zA-Z0-9_\.-]', '_', $folderName . "_video_01");   // Replace image name by game name (folder)
                    $productVideo1['name'] = strtolower($productVideo1['name']);
                    if (file_exists('product_pictures/' . $folderName . '/' . $productVideo1['name'])) {   // If file exists, rename it
                        $num = 1;
                        while (file_exists('product_pictures/' . $folderName . '/' . $productVideo1['name'] . "_$num." . $info['extension'])) {
                            $num++;
                        }
                        $productVideo1['name'] = $info['filename'] . "_$num." . $info['extension'];
                    } else{
                        $productVideo1['name'] = $productVideo1['name'] . "." . $info['extension'];
                    }
                }
            }
        }

        // url validation
        if(isset($postVars['url'])){
            if (preg_match('/^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&\/\/=]*)$/', $url) != 1) {
                array_push($errorsArray, "Invalid product URL");
                $postVars['url'] = "";
            }
        }


        if ($errorsArray) {
            return $view->render($response, 'addedit_product.html.twig', ['categoriesList' => $categoriesList, 'genresList' => $genresList, 'developersList' => $developersList, 'platformsList' => $platformsList, 'v' => $postVars, 'errorsArray' => $errorsArray]);
        } else { // insert record to database with secret
            $picture1path = 'product_pictures/' . $folderName . '/' . $productImage1['name'];
            $picture2path = 'product_pictures/' . $folderName . '/' . $productImage2['name'];
            $picture3path = 'product_pictures/' . $folderName . '/' . $productImage3['name'];
            $video1path = 'product_pictures/' . $folderName . '/' . 'video' . '/' . $productVideo1['name'];


            if (!move_uploaded_file($productImage1['tmp_name'], $picture1path)) {
                global $log;
                $log->error("Error moving uploaded file: " . print_r($productImage1, true));
                return $view->render($response, '/error_internal.htlm.twig');
            }

            if (!move_uploaded_file($productImage2['tmp_name'], $picture2path)) {
                global $log;
                $log->error("Error moving uploaded file: " . print_r($productImage2, true));
                return $view->render($response, '/error_internal.html.twig');
            }

            if (!move_uploaded_file($productImage3['tmp_name'], $picture3path)) {
                global $log;
                $log->error("Error moving uploaded file: " . print_r($productImage3, true));
                return $view->render($response, '/error_internal.html.twig');
            }


            if (!move_uploaded_file($productVideo1['tmp_name'], $video1path)) {
                global $log;
                $log->error("Error moving uploaded video file: " . print_r($productVideo1, true));
                return $view->render($response, '/error_internal.html.twig');
            }

            if(isset($productId)){
                DB::update('products', ['category' => $category, 'name' => $name, 'description' => $description, 'game_genre' => $game_genre, 'developer' => $developer, 
                'platform' => $platform, 'releaseDate' => $releaseDate, 'unitPrice' => $unitPrice, 'onSale' => $onSale, 'newPrice' => $newPrice, 'video' => $video1path, 
                'picture1' => $picture1path, 'picture2' => $picture2path, 'picture3' => $picture3path, 'url' => $url ], 'id=%s', $productId);

                $log->debug(sprintf("Product updated: id = %d, name = %s", $productId, $name));

            } else {
                DB::insert('products', ['category' => $category, 'name' => $name, 'description' => $description, 'game_genre' => $game_genre, 'developer' => $developer, 
                'platform' => $platform, 'releaseDate' => $releaseDate, 'unitPrice' => $unitPrice, 'onSale' => $onSale, 'newPrice' => $newPrice, 'video' => $video1path, 
                'picture1' => $picture1path, 'picture2' => $picture2path, 'picture3' => $picture3path, 'url' => $url ]);

                $newId = DB::insertId();    // Id of the new product
                $log->debug(sprintf("Product inserted: id = %d, name = %s", $newId, $name));
            }
        }
    }

    return $view->render($response, 'addedit_product.html.twig', ['categoriesList' => $categoriesList, 'genresList' => $genresList, 'developersList' => $developersList, 'platformsList' => $platformsList, 'v' => $postVars, 'errorsArray' => $errorsArray, 'newId' => $newId]);
});

