<?php

require_once "setup.php";

use DI\Container;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Hybridauth\Exception\Exception;
use Hybridauth\Hybridauth;
use Hybridauth\HttpClient;
use Hybridauth\Storage\Session;
use PhpParser\Node\Stmt\Echo_;

require_once "account.php";
require_once "admin.php";

$app->get('/error_internal', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_internal.html.twig');
});

$app->get('/error_forbidden', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_forbidden.html.twig');
});

$app->get('/error_notfound', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_notfound.html.twig');
});

$app->get('/underconstruction', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'error_underconstruction.html.twig');
});

// Index page ============================================================================================
$app->get('/', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    $productListTop = DB:: query("SELECT p.id as 'id', p.name as 'name', p.picture1 as 'picture1', p.description as 'description', averageRating, totalRatings
        from products as p Left join (select * , avg(rating) as averageRating, COUNT(productId) as totalRatings from reviews group by productId)
        as rat On productId = p.id ORDER BY averageRating DESC, totalRatings DESC LIMIT 3");

    foreach ($productListTop as &$product) {
        $product['description'] = strip_tags($product['description']/*, "<p><em><strong><i><b><h3><h4>"*/);   // First remove the tags. The argument after the comma are the tags THAT YOU ALLOW TO STAY
        $product['description'] = substr($product['description'], 0, 100) . (strlen($product['description']) > 100 ? "..." : "");
    }

    $productListBestSeller = DB::query("SELECT p.id as 'id', p.name as 'name', p.picture1 as 'picture1', p.description as 'description', averageRating, totalRatings, totalSales
        FROM products as p LEFT JOIN (SELECT * , avg(rating) as averageRating, COUNT(productId) as totalRatings FROM reviews GROUP BY productId)
        as r ON r.productId = p.id 
        LEFT JOIN (SELECT * , COUNT(productId), SUM(quantity) as totalSales FROM orderitems GROUP BY productId) as oi ON oi.productId = p.id
        ORDER BY totalSales DESC, averageRating DESC LIMIT 6");

    foreach ($productListBestSeller as &$product) {
        $product['description'] = strip_tags($product['description']/*, "<p><em><strong><i><b><h3><h4>"*/);
        $product['description'] = substr($product['description'], 0, 100) . (strlen($product['description']) > 100 ? "..." : "");
    }

    $productListUpcoming = DB::query("SELECT * FROM products WHERE releaseDate >= '2020-05-28' AND category = 'Games' ORDER BY releaseDate ASC LIMIT 3");   // May 28th is an arbitrary date. Could be any date in the future
    foreach ($productListUpcoming as &$product) {
        $product['description'] = strip_tags($product['description']/*, "<p><em><strong><i><b><h3><h4>"*/);
        $product['description'] = substr($product['description'], 0, 200) . (strlen($product['description']) > 200 ? "..." : "");
    }

    // Upcoming games from RAWG Database
    $upcoming = Unirest\Request::get(
        "https://rawg-video-games-database.p.rapidapi.com/games?dates=2020-05-01,2020-06-01&ordering=-added",
        array(
            "X-RapidAPI-Host" => "rawg-video-games-database.p.rapidapi.com",
            "X-RapidAPI-Key" => "be48712326msh46be95dcec14c57p1fbf17jsnaf3a49692ede"
        )
    );

    //This resturn an Unirest object, so we have to parse only the info that we need (in the body)
    $upcomingGames = $upcoming->body->results;
    $upcomingGames =  array_slice($upcomingGames, 0, 6); // to get first six only
    $upcomingGameList = [];

    foreach ($upcomingGames as $game) {
        $category = 'Games';
        $name = $game->name;
        $game_genre = $game->genres[0]->name;
        $platforms = [];
        foreach ($game->platforms as $platform) {
            array_push($platforms, $platform->platform->name);
        }
        $platform = implode(', ', $platforms);
        $releaseDate = $game->released;
        $picture1 = $game->background_image;
        $picture2 = $game->short_screenshots[0]->image;
        $picture3 = $game->short_screenshots[1]->image;

        array_push($upcomingGameList, array(
            'category' => $category, 'name' => $name, 'game_genre' => $game_genre, 'platform' => $platform, 
            'releaseDate' => $releaseDate, 'picture1' => $picture1, 'picture2' => $picture2, 'picture3' => $picture3 ));
    }

    return $view->render($response, 'index.html.twig', [
        'productListTop' => $productListTop, 'productListBestSeller' => $productListBestSeller,
        'productListUpcoming' => $productListUpcoming, 'upcomingGameList' => $upcomingGameList
    ]);
});

// Index page per Category ============================================================================================
$app->get('/category/{category}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    $category = '%' . $args['category'] . '%';
    $consoleCategory = $args['category'];

    // Retrieve products of the platform-family  
    $productListTop = DB:: query("SELECT p.id as 'id', p.name as 'name', p.picture1 as 'picture1', p.description as 'description', averageRating, 
        totalRatings from products as p Left join (select * , avg(rating) as averageRating, COUNT(productId) as totalRatings from reviews 
        group by productId) as rat On productId = p.id WHERE platform LIKE %s AND category = 'Games' ORDER BY averageRating DESC, 
        totalRatings DESC LIMIT 3", $category);

    foreach ($productListTop as &$product) {
        $product['description'] = strip_tags($product['description']/*, "<p><em><strong><i><b><h3><h4>"*/);   // First remove the tags. The argument after the comma are the tags THAT YOU ALLOW TO STAY
        $product['description'] = substr($product['description'], 0, 100) . (strlen($product['description']) > 100 ? "..." : "");
    }
  
    $productListBestSeller = DB::query("SELECT p.id as 'id', p.name as 'name', p.picture1 as 'picture1', p.description as 'description', averageRating, 
        totalRatings, totalSales FROM products as p LEFT JOIN (SELECT * , avg(rating) as averageRating, COUNT(productId) as totalRatings FROM reviews 
        GROUP BY productId) as r ON r.productId = p.id LEFT JOIN (SELECT * , COUNT(productId) as totalSales FROM orderitems GROUP BY productId) as oi ON oi.productId = p.id        
        WHERE platform LIKE %s AND category = 'Games' ORDER BY totalSales DESC, averageRating DESC LIMIT 6", $category);
    
    foreach ($productListBestSeller as &$product) {
        $product['description'] = strip_tags($product['description']/*, "<p><em><strong><i><b><h3><h4>"*/);
        $product['description'] = substr($product['description'], 0, 100) . (strlen($product['description']) > 100 ? "..." : "");
    }

    $productListUpcoming = DB::query("SELECT * FROM products WHERE releaseDate >= '2020-05-28' AND platform LIKE %s AND category = 'Games' ORDER BY releaseDate ASC LIMIT 3", $category);   // May 28th is an arbitrary date. Could be any date in the future
    foreach ($productListUpcoming as &$product) {
        $product['description'] = strip_tags($product['description']/*, "<p><em><strong><i><b><h3><h4>"*/);
        $product['description'] = substr($product['description'], 0, 200) . (strlen($product['description']) > 200 ? "..." : "");
    }

    $categoryConsoles = DB::query("SELECT * FROM products WHERE platform LIKE %s AND category = 'Consoles' ORDER BY id DESC LIMIT 3", $category);
    foreach ($categoryConsoles as &$product) {
        $product['description'] = strip_tags($product['description']/*, "<p><em><strong><i><b><h3><h4>"*/);
        $product['description'] = substr($product['description'], 0, 200) . (strlen($product['description']) > 200 ? "..." : "");
    }

    return $view->render($response, 'index.html.twig', [
        'productListTop' => $productListTop,
        'productListBestSeller' => $productListBestSeller,
        'productListUpcoming' => $productListUpcoming,
        'categoryConsoles' => $categoryConsoles,
        'category' => $consoleCategory
    ]);
});

// All products par Category ============================================================================================
$app->get('/category/list/{category}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    $category = '%' . $args['category'] . '%';

    // Retrieve all products of the platform-family
    $productList = DB::query("SELECT *, p.id as 'productId', averageRating, totalRatings FROM products AS p LEFT JOIN (SELECT * , AVG(rating) as averageRating, 
        COUNT(productId) as totalRatings FROM reviews GROUP BY productId) as rat On productId = p.id WHERE platform 
        LIKE %s AND category = 'Games' ORDER BY p.id DESC", $category);
    foreach ($productList as &$product) {
        $product['description'] = strip_tags($product['description']/*, "<p><em><strong><i><b><h3><h4>"*/);   // First remove the tags. The argument after the comma are the tags THAT YOU ALLOW TO STAY
        $product['description'] = substr($product['description'], 0, 100) . (strlen($product['description']) > 100 ? "..." : "");
}

    return $view->render($response, 'categorylist.html.twig', [ 'productList' => $productList ]);
});

// Searchbar ============================================================================================
$app->get('/search/{keyword}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    $keyword = htmlspecialchars($args['keyword']);  // To avoid input problems from user
    $keywordClean = '%' . $keyword . '%';

    // Retrieve all products related with the keyword
    $productList = DB::query("SELECT * ,  p.id as 'productId', averageRating, totalRatings FROM products AS p LEFT JOIN (SELECT * , AVG(rating) as averageRating, 
    COUNT(productId) as totalRatings FROM reviews GROUP BY productId) as rat On productId = p.id
                            WHERE (p.category LIKE %s OR
                                p.name LIKE %s OR
                                p.description LIKE %s OR
                                p.game_genre LIKE %s OR
                                p.developer LIKE %s OR
                                p.platform LIKE %s)                               
                            ORDER BY p.id DESC", $keywordClean, $keywordClean, $keywordClean, $keywordClean, $keywordClean, $keywordClean);
    foreach ($productList as &$product) {
        $product['description'] = strip_tags($product['description']/*, "<p><em><strong><i><b><h3><h4>"*/);   // First remove the tags. The argument after the comma are the tags THAT YOU ALLOW TO STAY
        $product['description'] = substr($product['description'], 0, 100) . (strlen($product['description']) > 100 ? "..." : "");
}

    return $view->render($response, 'searchlist.html.twig', [ 'productList' => $productList, 'keyword' => $keyword ]);
});

// Product page ============================================================================================
$app->map(['GET', 'POST'], '/product/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    // 1. fetch the article with author information
    $id = $args['id'];
    $product = DB::queryFirstRow("SELECT * FROM products WHERE id = %s", $id);
    
    if (!$product) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    }
    // 2. Check if comment is being received via post, verify it, insert to database if ok ,otherwise show error
    if ($request->getMethod() == 'POST') {
        //only for authenticated users
        if (!isset($_SESSION['user'])) {
            return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
        }
        $userId = $_SESSION['user']['id'];
        $review = DB::queryFirstRow("SELECT * FROM reviews WHERE productId = %s AND userId = %s", $id, $userId);
        if ($review) {
            return $view->render($response, 'review_write_notallowed.html.twig');
        }
        $postVars = $request->getParsedBody();
        $rating = $postVars['rating'];
        $content = $postVars['content'];
        $messages = array();
        if ($rating == null || $rating < 1 || $rating > 5) {
            $messages['rating'] = "You must use stars to rate this game";
        }
        if ($content == null || strlen($content) < 10 || strlen($content) > 10000) {
            $messages['content'] = "Comment must be 10-10000 characters long";
        }
        if ($messages) {     // true if array is not empty. STATE 2: Failed
            $reviewSent = false;
            // return $view->render($response, 'product.html.twig', [     //on failure, re-display
            //     'product' => $product, 'v' => $postVars, 'messages' => $messages, 'product' => $product, 'productRatings' => $productRatings, 'date' => $date, 'time' => $time, 'commentsList' => $commentsList,  'reviewSent' => $reviewSent
            // ]);
        } else { //STATE 3: Success
            DB::insert('reviews', ['productId' => $id, 'userId' => $userId, 'rating' => $rating, 'content' => $content]);    // Meekrodb handles all errors, protection, etc. You only need this line to send to Database
            $reviewSent = true;
            $postVars = null;
            //return $view->render($response, 'product.html.twig', ['product' => $product, 'productRatings' => $productRatings, 'date' => $date, 'time' => $time, 'commentsList' => $commentsList,  'reviewSent' => $reviewSent]);
        }
    }
    // 3. fetch product average rating and comments, and display them
    $productRatings = DB::queryFirstField("SELECT AVG(rating) FROM reviews WHERE productId=%s;", $id);
    $productRatings = number_format((float)$productRatings, 1, '.', '');
    

    $reviews = DB::query("SELECT r.id, r.productId, r.userId, r.rating, r.content, r.creationTS, u.name as username, u.picturePath as authorPicture
            FROM reviews as r JOIN users as u ON r.userId = u.id WHERE r.productId = %d", $id);
    $commentsList = [];
    if (!$reviews) {
        $date = $time = "";
    } else {
        foreach ($reviews as &$review) {
            $author = $review['username'];
            $creationTS = $review['creationTS'];
            $authorPicture = $review['authorPicture'];
            $date = date('M d, Y', strtotime($creationTS));
            $time = date('H:i', strtotime($creationTS));
            $productComment = strip_tags($review['content'], "<p><ul><li><ol><em><strong><i><b><h3><h4><span>");
            $reviewRating = $review['rating'];
            $reviewId = $review['id'];
            array_push($commentsList, ['author' => $author, 'authorPicture' => $authorPicture, 'date' => $date, 'time' => $time, 'productComment' => $productComment, 'reviewRating' => $reviewRating, 'reviewId' => $reviewId]);
        }
    }
    //$productRatings = DB::query("SELECT rating FROM reviews WHERE productId = %d ORDER BY id DESC", $id); /*DELETE LATER!! Testing purposes only*/
    return $view->render($response, 'product.html.twig', ['product' => $product, 'productRatings' => $productRatings, 'date' => $date, 'time' => $time, 'commentsList' => $commentsList,  'v' => $postVars, 'messages' => $messages, 'reviewSent' => $reviewSent]);
});


// Cart page =========================================================================================

$app->map(['GET', 'POST'], '/mycart[/{productId:[0-9]+}/{quantity:[0-9]+}/{unitPrice}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    $cartitemList = DB::query(
        "SELECT c.id as cartId, p.id as productId, c.quantity, p.name, p.description, p.category, p.game_genre, p.developer, p.platform, p.picture1, p.unitPrice, p.onSale, p.newPrice
                        FROM cartitems as c, products as p
                        WHERE c.productId = p.Id AND sessionId=%s",
        session_id()
    );


    if ($request->getMethod() == 'POST') {
        //only for authenticated users
        if (!isset($_SESSION['user'])) {
            return $response->withHeader('Location', '/error_forbidden')->withStatus(302);
        }

        $productId = $args('productId');
        $quantity = $args('quantity');
        $unitPrice = $args('unitPrice');
        // Check if product exists in the cart (update), if not, add it.
        $item = DB::queryFirstRow("SELECT * FROM cartitems WHERE productId=%d AND sessionId=%s", $productId, session_id());
        if ($item) {
            DB::update(
                'cartitems',
                array('sessionId' => session_id(), 'productId' => $productId, 'quantity' => $item['quantity'] + $quantity, 'unitPrice' => $unitPrice),
                "productId=%d AND sessionId=%s",
                $productId,
                session_id()
            );
        } else {
            DB::insert('cartitems', array('sessionID' => session_id(), 'productId' => $productId, 'quantity' => $quantity, 'unitPrice' => $unitPrice));
        }
        // show current contents of the cart
        $cartitemList = DB::query(
            "SELECT c.id as cartId, p.id as productId, c.quantity, p.name, p.description, p.category, p.game_genre, p.developer, p.platform, p.picture1, p.unitPrice, p.onSale, p.newPrice
                        FROM cartitems as c, products as p
                        WHERE c.productId = p.Id AND sessionId=%s",
            session_id()
        );
    }
    // echo '<pre>'; print_r($cartitemList);die();
    return $view->render($response, 'cart.html.twig', array('cartitemList' => $cartitemList));
});

// AJAX call, not used directy by user
$app->post('/cart/add/{productId:[0-9]+}/{quantity:[0-9]+}/{unitPrice}', function (Request $request, Response $response, array $args) {

    // Check if user is logged in
    $userId = $_SESSION['user'] ? $_SESSION['user']['id'] : NULL;
    if ($userId === NULL) {
        $response->getBody()->write(json_encode('You must login to add a product to cart!')); // User not logged in
        return $response;
    }
    $productInCart = DB::queryFirstRow("SELECT id FROM cartitems WHERE productId = %d AND sessionId = %s", $args['productId'], session_id());
    if (!isset($productInCart)) {
        $cartItem = [
            'quantity' => $args['quantity'],
            'productId' => $args['productId'],
            'unitPrice' => $args['unitPrice'],
            'sessionId' => session_id()
        ];
        DB::insert('cartitems', $cartItem);
        //Just to return something (JS dont like when we dont return anything):
        $response->getBody()->write(json_encode('Product added to cart!')); //
    } else {
        $response->getBody()->write(json_encode('Product already in cart!')); // Already in cart
    }
    return $response;
});

// AJAX call, not used directy by user
$app->post('/mycart/update/{cartItemId:[0-9]+}/{productId:[0-9]+}/{quantity:[0-9]+}/{unitPrice}', function (Request $request, Response $response, array $args) {

    $cartItemId = $args['cartItemId'];
    $productId = $args['productId'];
    $quantity = $args['quantity'];
    $unitPrice = $args['unitPrice'];
    // Check if user is logged in
    $userId = $_SESSION['user'] ? $_SESSION['user']['id'] : NULL;
    if ($userId === NULL) {
        $response->getBody()->write(json_encode('You must login to modify a product in the cart!'));
        return $response;
    }

    if (isset($quantity) && isset($productId)) {
        if ($quantity == 0) {
            DB::query("DELETE FROM cartitems WHERE productId=%d AND sessionId=%s", $productId, session_id());
        } else {
            $cartItem = [
                'quantity' => $args['quantity'],
                'productId' => $args['productId'],
                'unitPrice' => $args['unitPrice'],
                'sessionId' => session_id()
            ];
            DB::update('cartitems', $cartItem, "id=%d", $cartItemId);
        }
        //Just to return something (JS dont like when we dont return anything):
        $response->getBody()->write(json_encode('succeed')); //
    } else {
        $response->getBody()->write(json_encode('fail')); // Incomplete information given
    }
    return $response;
});


// AJAX call, not used directy by user
$app->get('/cart/getquantity', function (Request $request, Response $response, array $args) {
    $count = DB::queryFirstField("SELECT COUNT(*) FROM cartitems WHERE sessionId = %s", session_id());
    $json = json_encode($count);
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $response->getBody()->write($json);
    return $response;
});

// Order page =========================================================================================

$app->map(['GET', 'POST'], '/order', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    $totalNumberOfItems = DB::queryFirstField("SELECT COUNT(*)
                                            FROM cartitems
                                            WHERE cartitems.sessionId = %s", session_id());

    $totalBeforeTax = DB::queryFirstField("SELECT SUM(products.unitPrice * cartitems.quantity)
                                            FROM cartitems, products
                                            WHERE cartitems.sessionId = %s AND cartitems.productId = products.Id", session_id());
    // TODO: properly compute taxes, shipping, ...
    $shippingBeforeTax = 10.00;
    $taxes = ($totalBeforeTax + $shippingBeforeTax) * 0.15;
    $totalWithShippingAndTaxes = number_format($totalBeforeTax + $shippingBeforeTax + $taxes, 2);
    $orderDate = strtotime('now');
    $dueDate = date('Y-m-d H:i:s', strtotime($orderDate . '+3 day'));      // Devilery in 3 days
    $status = 'Placed';

    if ($request->getMethod() == 'POST') {

        $postVars = $request->getParsedBody();

        // Sanitize POST Array
        $postVars = filter_var_array($postVars, FILTER_SANITIZE_STRING);

        $name = $postVars['name'];
        $address = $postVars['address'];
        $postalCode = $postVars['postalCode'];
        $province = $postVars['province'];
        $city = $postVars['city'];
        $country = $postVars['country'];
        $email = $postVars['email'];
        $phone = $postVars['phone'];
        $cardName = $postVars['cardName'];
        $cardAddress = $postVars['cardAddress'];
        $cardPostalCode = $postVars['cardPostalCode'];
        $cardProvince = $postVars['cardProvince'];
        $cardCity = $postVars['cardCity'];
        $cardCountry = $postVars['cardCountry'];
        $token = $postVars['stripeToken'];   // token given by stripe if credit card info is approved

        $deliveryMethod = $postVars['deliveryMethod'];
        $dueDate = date('Y-m-d H:i:s');
        $status = 'Placed';
        $valueList = array(
            'name' => $name,
            'address' => $address,
            'postalCode' => $postalCode,
            'province' => $province,
            'city' => $city,
            'country' => $country,
            'email' => $email,
            'phone' => $phone
        );            // We're not sending back the credit card information (To avoid security problems)

        // FIXME: verify inputs - MUST DO IT IN A REAL SYSTEM
        $errorList = array();
        //

        if ($errorList) {
            return $view->render($response, 'order.html.twig', array(
                'totalBeforeTax' => number_format($totalBeforeTax, 2),
                'shippingBeforeTax' => number_format($shippingBeforeTax, 2), 'taxes' => number_format($taxes, 2),
                'totalWithShippingAndTaxes' => number_format($totalWithShippingAndTaxes, 2), 'v' => $valueList
            ));
        } else {

            if (isset($token)) {

                // To be able to charge:
                // Add stripe-customer-id to the database, into users table (user already exist in this implementation)
                $client = DB::queryFirstRow("SELECT id, stripeCustId FROM users WHERE email = %s", $email);
                if (!$client) {
                    $clientNotFound = true;
                }
                if (isset($client)) {
                    $stripeCustId = $client['stripeCustId'];

                    if (preg_match('//', ($client['stripeCustId'])) == 1) {           // If the user does not have a stripe-customer-id
                        // 1. Create Customer In Stripe
                        $stripeCust = \Stripe\Customer::create(array(
                            "name" =>  $cardName,
                            "address" => [
                                "line1" => $cardAddress,
                                "postal_code" => $cardPostalCode,
                                "state" => $cardProvince,
                                "city" => $cardCity,
                                "country" => "CA"
                            ], // FIXME! 2-letter country code (ISO 3166-1 alpha-2)
                            "email" => $email,
                            "phone" => $phone,
                            "source" => $token
                        ));
                        $stripeCustId = $stripeCust['id'];
                        // 2. Add the stripeCust id value into the DB
                        DB::update('users', ['stripeCustId' => $stripeCustId], "email=%s", $email);
                    }

                    // 3. Charge Customer
                    $amount = $totalWithShippingAndTaxes * 100;

                    $charge = \Stripe\Charge::create(array(
                        "amount" => $amount,                                            // price of the product. In Stripe we dont put the '.', so in this case 5000 = $50.00
                        "currency" => "cad",                                            // cad, usd, etc
                        "description" => "Game Store - Standard Online Sell",           // not required, but will show on the back end
                        "customer" => $stripeCustId                                     // customer who bought it
                    ));

                    // Define some variables:
                    $tid = $charge['id'];                   // transaction Id
                    $description = $charge['description'];   // product description

                    // 4. Try to insert the data into the DB
                    try {
                        DB::insert('orders', array(
                            'id' => $tid,
                            'userId' => $_SESSION['user'] ? $_SESSION['user']['id'] : NULL,
                            'firstLastName' => $name,
                            'address' => $address,
                            'postalCode' => $postalCode,
                            'phoneNo' => $phone,
                            'email' => $email,
                            'deliveryMethod' => $deliveryMethod,
                            'deliveryCost' => $shippingBeforeTax,
                            "dueDate" => $dueDate,
                            'totalAmountBeforeTax' => $totalBeforeTax,
                            'taxAmount' => $taxes,
                            'totalPaid' => $totalWithShippingAndTaxes,
                            'currency' => $charge['currency'],
                            'orderStatus' => $status,
                            'userSCid' => $stripeCustId,
                            'paymentStatus' => $charge['status']
                        ));
                        $orderId = $tid;
                        // 5. copy all records from cartitems to 'orderitems' (select & insert)
                        $cartitemList = DB::query("SELECT c.productId as productId, c.quantity as quantity, c.unitPrice as unitPrice, p.name as name, p.description as description
                                                    FROM cartitems as c, products as p
                                                    WHERE c.productId = p.Id AND sessionId=%s", session_id());

                        // 5a. add orderId to every sub-array (element) in $cartitemList
                        array_walk($cartitemList, function (&$item, $key) use ($orderId) {
                            $item['orderId'] = $orderId;
                        });

                        DB::insert('orderitems', $cartitemList);

                        // 6. delete cartitems for this user's session (delete)
                        DB::delete('cartitems', "sessionId=%s", session_id());

                        // TODO: send a confirmation email
                        /*
                        $emailHtml = $app->view()->getEnvironment()->render('email_order.html.twig');
                        $headers = "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
                        mail($email, "Order " .$orderID . " placed ", $emailHtml, $headers);
                        */
                        //
                        return $view->render($response, 'order_success.html.twig', ['tid' => $tid, 'description' => $description]);
                    } catch (MeekroDBException $e) {
                        DB::rollback();
                        //sql_error_handler(array('error' => $e->getMessage(), 'query' => $e->getQuery() )); // This gives an error. FIXME
                    }
                } 
            }
        }
    }

    return $view->render($response, 'order.html.twig', array(
        'totalNumberOfItems' => $totalNumberOfItems, 'totalBeforeTax' => number_format($totalBeforeTax, 2),
        'shippingBeforeTax' => number_format($shippingBeforeTax, 2), 'taxes' => number_format($taxes, 2),
        'totalWithShippingAndTaxes' => number_format($totalWithShippingAndTaxes, 2),
        'clientNotFound' => $clientNotFound
    ));
});

// Order success page =========================================================================================

$app->get('/success', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    $postVars = $request->getParsedBody();
    $tid = $postVars['tid'];
    $description = $postVars['description'];

    return $view->render($response, 'order_success.html.twig', ['tid' => $tid, 'description' => $description]);
});


// Session page ============================================================================================
// for debugging purposes only
$app->get('/session', function (Request $request, Response $response, array $args) {
    global $config;
    $body = "<pre>\n\$_SESSION:\n" . "session: " . print_r($_SESSION, true) . "\n\n=======================================\n\n" .
        "session id:\n" . session_id() . "\n\n=======================================\n\n" .
        "<pre>\n\$config:\n" . print_r($config, true);
    $response->getBody()->write($body);
    return $response;
});

// RAWG Video Game Database test page =========================================================================================

$app->get('/rawg', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);

    // Upcoming games from RAWG Database
    $upcoming = Unirest\Request::get(
        "https://rawg-video-games-database.p.rapidapi.com/games?dates=2020-05-01,2020-06-01&ordering=-added",
        array(
            "X-RapidAPI-Host" => "rawg-video-games-database.p.rapidapi.com",
            "X-RapidAPI-Key" => "be48712326msh46be95dcec14c57p1fbf17jsnaf3a49692ede"
        )
    );

    //This resturn an Unirest object, so we have to parse only the info that we need (in the body)
    $upcomingGames = $upcoming->body->results;
    $upcomingGamesList = [];

    foreach ($upcomingGames as $game) {
        $category = 'Games';
        $name = $game->name;
        $description = '-';
        $game_genre = $game->genres[0]->name;
        $developer = '-';
        $platforms = [];
        foreach ($game->platforms as $platform) {
            array_push($platforms, $platform->platform->name);
        }
        $platform = implode(', ', $platforms);
        $releaseDate = $game->released;
        $unitPrice = "-";
        $onSale = "-";
        $newPrice = "-";
        //$video = $game->clip->clip;
        $picture1 = $game->background_image;
        $picture2 = $game->short_screenshots[0]->image;
        $picture3 = $game->short_screenshots[1]->image;
        $url = '-';

        array_push($upcomingGamesList, array(
            'category' => $category, 'name' => $name, 'description' => $description,
            'game_genre' => $game_genre, 'developer' => $developer, 'platform' => $platform, 'releaseDate' => $releaseDate,
            'unitPrice' => $unitPrice, 'onSale' => $onSale, 'newPrice' => $newPrice, /*'video' => $video,*/ 'picture1' => $picture1,
            'picture2' => $picture2, 'picture3' => $picture3, 'url' => $url
        ));
    }

    echo "<pre>";
    print_r($upcomingGamesList);
    die();

    return $view->render($response, 'order_success.html.twig', ['upcomingGamesList' => $upcomingGamesList]);
});


$app->run();
