<?php

use DI\Container;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Hybridauth\Exception\Exception;
use Hybridauth\Hybridauth;
use Hybridauth\HttpClient;
use Hybridauth\Storage\Session;

require_once "setup.php";

// Register page =========================================================================================================
// STATE 1: first show of the form
$app->get('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/register', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    // extract submitted values
    $postVars = $request->getParsedBody();
    $username = $postVars['username'];
    $name = $postVars['name'];
    $email = $postVars['email'];
    $password = $postVars['password1'];
    $password2 = $postVars['password2'];
    // check validity
    $messages = array();
    $usernameQuality = verifyUsernameQuality($username);
    if ($usernameQuality !== TRUE) {
        $messages["username"] = $usernameQuality;
        $postVars['username'] = "";
    }
    $nameQuality = verifyNameQuality($name);
    if ($nameQuality !== TRUE) {
        $messages["name"] = $nameQuality;
        $postVars['name'] = "";
    }
    $emailQuality = verifyEmailQuality($email);
    if ($emailQuality === 'emailNotConfirmed') {
        return $view->render($response, 'register_confirmation.html.twig', ['v' => $postVars, 'emailNotConfirmed' => true]);
    }
    if ($emailQuality !== TRUE) {
        $messages["email"] = $emailQuality;
        $postVars['email'] = "";
    }
    $passQuality = verifyPasswordQuality($password);
    if ($passQuality !== TRUE) {
        $messages["password"] = $passQuality;
    } else if (empty($password2) || $password2 != $password) {
        $messages["noMatch"] = "Passwords must match";
    }
    // Collect image information
    $directory = $this->get('upload_directory');
    $uploadedFiles = $request->getUploadedFiles();
    // handle single input with single file upload
    $uploadedFile = $uploadedFiles['picture'];
    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
        $filepath = moveUploadedFile($username, $directory, $uploadedFile);
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        if ($extension != 'png' && $extension != 'gif' && $extension != 'jpg') {
            $messages["picture"] = "Upload a valid image (jpg, png, gif)";
        }
    } else {
        $filepath = "/uploads/default.png";
    }
    if ($messages) {
        return $view->render($response, 'register.html.twig', ['v' => $postVars, 'messages' => $messages]);
    } else { // insert record to database with secret
        $secret = generateRandomString(60);
        $creationDateTime = gmdate("Y-m-d H:i:s"); // GMT time zone
        // encrypt password before insert
        $password = hash('sha256', $password, false);
        DB::insert('users', ['username' => $username, 'name' => $name, 'email' => $email, 'password' => $password, 'picturePath' => $filepath, 'secret' => $secret, 'creationDateTime' => $creationDateTime]);
        // send confirmation email to user
        $isEmailSent = sendConfirmationEmail($email, $secret, $username);
        if ($isEmailSent == false) {
            return $response->withHeader("Location", "/error_internal", 403);
        } else {
            return $view->render($response, 'register_confirmation.html.twig', ['v' => $postVars, 'message' => $messages]);
        }
    }
});


// Registration confirmation page =========================================================================================
$app->get('/registerconfirmation/{secret}', function (Request $request, Response $response, array $args) {
    global $log;
    $view = Twig::fromRequest($request);
    $secret = $args['secret'];
    $confirmUser = DB::queryFirstRow("SELECT * FROM users WHERE secret=%s", $secret);
    if (!$confirmUser) {
        $log->debug(sprintf('Confirmation token not found, token=%s', $secret));
        return $view->render($response, 'register_confirmation_notfound.html.twig');
    }
    // check if confirmation link has not expired
    $creationDT = strtotime($confirmUser['creationDateTime']); // convert to seconds since Jan 1, 1970 (UNIX time)
    $nowDT = strtotime(gmdate("Y-m-d H:i:s")); // current time GMT
    $diff = abs($nowDT - $creationDT);
    if ($diff > 60 * 60 * 60) { // expired
        $log->debug(sprintf('Confirmation token expired userid=%s, token=%s', $confirmUser['id'], $secret));
        return $view->render($response, 'register_confirmation_notfound.twig');
    }
    // update secret to null (activate user)
    DB::update('users', ['secret' => null], "id=%d", $confirmUser['id']);
    return $view->render($response, 'register_confirmation_success.html.twig', ['user' => $confirmUser]);
});

$app->get('/confirmationNotFound', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register_confirmation_notfound.html.twig');
});

// Resend confirmation link ===============================================================================================
// STATE 1: first show of the form
$app->get('/resendConfirmation', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'register_confirmation_resend.twig');
});

$app->post('/resendConfirmation', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postVars = $request->getParsedBody();
    $email = $postVars['email'];
    // check if email exist on database
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if (!$user) { // email not found
        return $view->render($response, 'register_confirmation_resend.twig', ['emailNotFound' => true]);
    } else if ($user && $user['secret'] == null) { // email already confirmed
        return $view->render($response, 'register_confirmation_success', ['user' => $user]);
    } else { // email not confirmed
        // create new secret and resend confirmation email
        $secret = generateRandomString(60);
        $creationDateTime = gmdate("Y-m-d H:i:s"); // GMT time zone
        DB::update('users', ['secret' => $secret, 'creationDateTime' => $creationDateTime], "id=%d", $user['id']);
        sendConfirmationEmail($email, $secret, $user);
        return $view->render($response, 'register_confirmation.html.twig', ['v' => $postVars, 'emailNotConfirmed' => true]);
    }
});

// Reset Password =========================================================================================================
$app->map(['GET', 'POST'], '/internpassresetaction/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $userId = $args['id'];
    $user = DB::queryFirstRow("SELECT * FROM users WHERE id = %s", $userId);
    if (!$user) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    }
    if ($request->getMethod() == 'POST') {
        $post = $request->getParsedBody();
        $password = $post['password'];
        $pass1 = $post['newPass1'];
        $pass2 = $post['newPass2'];
        $passEnc = hash('sha256', $password, false);
        if ($passEnc != $user['password']) {
            return $view->render($response, 'password_reset_intern.html.twig', ['wrongPassword' => TRUE]);
        }
        $passQuality = verifyPasswordQuality($pass1);
        if ($passQuality !== TRUE) {
            return $view->render($response, 'password_reset_intern.html.twig', ['passQuality' => $passQuality]);
        }
        if ($pass1 != $pass2) {
            return $view->render($response, 'password_reset_intern.html.twig', ['noMatch' => TRUE]);
        } else {
            // encrypt password before insert
            $newPassword = hash('sha256', $pass1, false);
            DB::update('users', ['password' => $newPassword], "id=%d", $userId);
            return $view->render($response, 'password_reset_action_success.html.twig');
        }
    } else {
        return $view->render($response, 'password_reset_intern.html.twig');
    }
});

$app->get('/passreset_request', function (Request $request, Response $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'password_reset.html.twig');
});

$app->post('/passreset_request', function (Request $request, Response $response) {
    global $log;
    $view = Twig::fromRequest($request);
    $post = $request->getParsedBody();
    $email = filter_var($post['email'], FILTER_VALIDATE_EMAIL); // 'FALSE' will never be found anyway
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if ($user && $user['provider'] === null) { //send email  if user is not register by social media //
        $secret = generateRandomString(60);
        $dateTime = gmdate("Y-m-d H:i:s"); // GMT time zone
        DB::insertUpdate('passwordresets', [
            'userId' => $user['id'],
            'secret' => $secret,
            'creationDateTime' => $dateTime
        ], [
            'secret' => $secret,
            'creationDateTime' => $dateTime
        ]);
        $isEmailSent = sendPasswordReset($email, $secret, $user);
        if ($isEmailSent == false) {
            return $response->withHeader("Location", "/error_internal", 403);
        } else {
            return $view->render($response, 'password_reset_sent.html.twig');
        }
    }
    return $view->render($response, 'password_reset_sent.html.twig');
});

$app->map(['GET', 'POST'], '/passresetaction/{secret}', function (Request $request, Response $response, array $args) {
    global $log;
    $view = Twig::fromRequest($request);
    $secret = $args['secret'];
    $resetRecord = DB::queryFirstRow("SELECT * FROM passwordresets WHERE secret=%s", $secret);
    if (!$resetRecord) {
        $log->debug(sprintf('Password reset token not found, token=%s', $secret));
        return $view->render($response, 'password_reset_action_notfound.html.twig');
    }
    // check if password reset has not expired
    $creationDT = strtotime($resetRecord['creationDateTime']); // convert to seconds since Jan 1, 1970 (UNIX time)
    $nowDT = strtotime(gmdate("Y-m-d H:i:s")); // current time GMT
    $diff = abs($nowDT - $creationDT);
    if ($diff > 60 * 60 * 60) { // expired
        DB::delete('passwordresets', 'secret=%s', $secret);
        $log->debug(sprintf('password reset token expired userid=%s, token=%s', $resetRecord['userId'], $secret));
        return $view->render($response, 'password_reset_action_notfound.html.twig');
    }
    if ($request->getMethod() == 'POST') {
        $post = $request->getParsedBody();
        $pass1 = $post['pass1'];
        $pass2 = $post['pass2'];
        $passQuality = verifyPasswordQuality($pass1);
        if ($passQuality !== TRUE) {
            return $view->render($response, 'password_reset_action.html.twig',  ['passQuality' => $passQuality]);
        }
        if ($pass1 != $pass2) {
            return $view->render($response, 'password_reset_action.html.twig', ['noMatch' => true]);
        } else {
            // encrypt password before insert
            $password = hash('sha256', $pass1, false);
            DB::update('users', ['password' => $password], "id=%d", $resetRecord['userId']);
            DB::delete('passwordresets', 'secret=%s', $secret); // cleanup the record
            return $view->render($response, 'password_reset_action_success.html.twig');
        }
    } else {
        return $view->render($response, 'password_reset_action.html.twig');
    }
});

// LOGIN ==================================================================================================================
// STATE 1: first show of the form
$app->get('/login[/{provider:Google|Facebook|Twitter}]', function (Request $request, Response $response, array $args) {
    global $provider;
    global $hybridauth;
    global $log;
    global $adapter;
    $view = Twig::fromRequest($request);
    if (!isset($args['provider'])) {
        return $view->render($response, 'login.html.twig');
    } else {
        $provider = $args['provider'];
        try {
            $log->debug(sprintf("User is not connected yet" . $provider));
            $_SESSION['provider'] = $provider;
            //Attempt to authenticate users with a provider by name (Facebook/Google/Twitter)
            $adapter = $hybridauth->authenticate($provider);
        } catch (\Exception $e) {
            //echo 'Oops, we ran into an issue! ' . $e->getMessage();
            return $view->render($response, 'login.html.twig', ['loginSocialMediaFailed' => TRUE]);
        }
    }
    $response = $response->withStatus(302);
    return $response->withHeader('Location', '/login/socialmedia');
});

// STATE 2&3: receiving submission
$app->post('/login', function (Request $request, Response $response, array $args) {
    global $userSession;
    $view = Twig::fromRequest($request);
    $postVars = $request->getParsedBody();
    $email = $postVars['email'];
    $password = $postVars['password'];
    // encrypt password before check it
    $passEnc = hash('sha256', $password, false);
    // check validity
    $loginSuccessful = false;
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if ($user) {
        if ($user['secret'] != null) {
            return $view->render($response, 'register_confirmation_resend.twig');
        }
        if ($user['password'] == $passEnc) {
            $loginSuccessful = true;
        }
    }
    if (!$loginSuccessful) { // STATE 2: Failed submission
        return $view->render($response, 'login.html.twig', ['error' => true]);
    } else { // STATE 3: Successful submission
        unset($user['password']); // remove password from array for security reasons
        $_SESSION['user'] = $user;
        return $view->render($response, 'login_success.html.twig', ['user' => $user]);
    }
});

// create a log channel  DEBUG SOCIAL MEDIA!
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/debug.log', Logger::DEBUG));

$app->get('/login/socialmedia', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    global $provider;
    global $userProfile;
    global $log;
    global $adapter;
    global $hybridauth;
    try {
        $provider = $_SESSION['provider'];
        $adapter = $hybridauth->authenticate($provider);
        $isConnected = $adapter->isConnected();
        //echo $isConnected;
        $log->debug(sprintf("User is connected"));
        //Retrieve the user's profile
        $userProfile = $adapter->getUserProfile();
        //Inspect profile's public attributes
        if (empty($userProfile)) {
            return $response->withHeader('Location', '/error_notfound')->withStatus(302);
        }
        //var_dump($userProfile);
        $identifier = $userProfile->identifier;
        //echo $identifier;
        // Check if a HybridAuth identifier already exists in DB
        $where = new WhereClause('and'); // create a WHERE statement of pieces joined by ANDs
        $where->add('identifier=%s', $identifier);
        $where->add('provider=%s', $provider);
        $user = DB::queryFirstRow("SELECT * FROM users WHERE %l", $where);
        if ($user) { // user is already registered, login
            $_SESSION['user'] = $user;
        } else { // insert new user to database
            $email = isset($userProfile->email) ? $userProfile->email : null; // twitter does not provide email          
            $photoURL = isset($userProfile->photoURL) ? $userProfile->photoURL : null;
            $username = isset($userProfile->firstName) ? $userProfile->firstName : "null"; // google does not provide name
            if ($provider == "Google") {
                $username = strstr($email, '@', true);
            }
            $name = isset($userProfile->firstName) && isset($userProfile->lastName) ? ($userProfile->firstName . " " . $userProfile->lastName) : null;
            // check if email is already registered on database (if email is provided)
            if ($email) {
                $userEmail = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
                if ($userEmail) {
                    return $view->render($response, 'login.html.twig', ['emailAlreadyRegistered' => TRUE]);
                }
            }
            DB::insert('users', ['username' => $username, 'name' => $name, 'email' => $email, 'picturePath' => $photoURL, 'identifier' => $identifier, 'provider' => $provider]);
            $newUserId = DB::insertId();
            $user = DB::queryFirstRow("SELECT * FROM users WHERE id=%s", $newUserId);
            $_SESSION['user'] = $user;
        }
        //Disconnect the adapter 
        $adapter->disconnect();
        return $view->render($response, 'login_success.html.twig', ['user' => $user]);
    } catch (\Exception $e) {
        //echo 'Oops, we ran into an issue! ' . $e->getMessage();
        return $view->render($response, 'login.html.twig', ['loginSocialMediaFailed' => TRUE]);
    }
});

// PROFILE==================================================================================================================
$app->get('/userprofile/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $id = $args['id'];
    $user = DB::queryFirstRow("SELECT * FROM users WHERE id = %s", $id);
    if (!$user) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    }
    return $view->render($response, 'userprofile.html.twig', ['user' => $user]);
});

$app->post('/userprofile/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $postVars = $request->getParsedBody();
    $userId = $args['id'];
    $user = DB::queryFirstRow("SELECT * FROM users WHERE id = %s", $userId);
    if (!$user) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    }
    $changes = $postVars['changes'];
    $messages = array();
    if ($changes == "changesLogin") {
        // handle the first form 
        $username = $postVars['username'];
        $name = $postVars['name'];
        $email = $postVars['email'];
        // validate fields
        if (preg_match('/^[a-zA-Z0-9_]{5,50}$/', $username) != 1) {
            $messages["username"] = "Username must be 5-50 characters long, and only consist of letters, digits, and underscores";
        } else { // check if username exist on database
            $usernameAlreadyRegistered = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);
            if ($usernameAlreadyRegistered && $username !== $user['username']) {
                $messages["username"] = "Username already registered, try a different one";
            }
        }
        $nameQuality = verifyNameQuality($name);
        if ($nameQuality !== TRUE) {
            $messages["name"] = $nameQuality;
        }
        if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
            $messages["email"] = "Invalid email";
        }
        $emailAlreadyRegistered = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
        if ($emailAlreadyRegistered && $email !== $user['email']) {
            $messages["email"] = "Email already registered, try a different one";
        }
        // Collect image information
        $directory = $this->get('upload_directory');
        $uploadedFiles = $request->getUploadedFiles();
        $uploadedFile = $uploadedFiles['picture'];
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filepath = moveUploadedFile($username, $directory, $uploadedFile);
            $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
            if ($extension != 'png' && $extension != 'gif' && $extension != 'jpg') {
                $messages["picture"] = "Upload a valid image (jpg, png, gif)";
            }
        } else {
            $filepath = $user['picturePath'];
        }
        if ($messages) {
            return $view->render($response, 'userprofile.html.twig', ['user' => $user, 'messages' => $messages]);
        } else {
            if ($email != $user['email']) { // user is registering a new email, so it must be verified
                $secret = generateRandomString(60);
                $creationDateTime = gmdate("Y-m-d H:i:s"); // GMT time zone
                DB::update('users', ['username' => $username, 'name' => $name, 'email' => $email, 'picturePath' => $filepath, 'secret' => $secret, 'creationDateTime' => $creationDateTime], "id=%s", $userId);
                // send confirmation email to user
                $isEmailSent = sendConfirmationEmail($email, $secret, $username);
                if ($isEmailSent == false) {
                    return $response->withHeader("Location", "/error_internal", 403);
                } else {
                    return $view->render($response, 'register_confirmation.html.twig', ['v' => $postVars]);
                }
            } else {
                DB::update('users', ['username' => $username, 'name' => $name, 'email' => $email, 'picturePath' => $filepath], "id=%s", $userId);
                $updatedUser = DB::queryFirstRow("SELECT * FROM users WHERE id = %s", $userId);
                $_SESSION['user'] = $updatedUser;
                return $view->render($response, 'userprofile.html.twig', ['user' => $updatedUser, 'profileUpdated' => TRUE]);
            }
        }
    } else if ($changes == "changesAddress") {
        // handle the second form
        $address = $postVars['address'];
        $apartment = $postVars['apartment'];
        $city = $postVars['city'];
        $province = $postVars['province'];
        $phoneNo = $postVars['phoneNo'];
        $zipcode = $postVars['zipCode'];
        if ($address) {
            if (strlen($address) < 6 || strlen($address) > 100) {
                $messages['address'] = "Address must be 6-100 characters long";
            }
        }
        if ($apartment) {
            if (strlen($apartment) > 20) {
                $messages['apartment'] = "Apartment must be maximum 20 characters long";
            }
        }
        if ($city) {
            if (strlen($city) > 100) {
                $messages['city'] = "City must be maximum 100 characters long";
            }
        }
        if ($phoneNo) {
            if (preg_match('/^[0-9_]+$/', $phoneNo) != 1) {
                $messages['phoneNo'] = "PhoneNo is not valid";
            }
        }
        if ($zipcode) {
            if (preg_match('/^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/', $zipcode) != 1) {
                $messages['zipCode'] = "ZipCode is not valid";
            }
        }
        if ($messages) {
            return $view->render($response, 'userprofile.html.twig', ['user' => $user, 'messages' => $messages]);
        } else {
            DB::update('users', ['address' => $address, 'apartment' => $apartment, 'city' => $city, 'province' => $province, 'phoneNo' => $phoneNo, 'zipcode' => $zipcode], "id=%s", $userId);
            $updatedUser = DB::queryFirstRow("SELECT * FROM users WHERE id = %s", $userId);
            return $view->render($response, 'userprofile.html.twig', ['user' => $updatedUser, 'addressUpdated' => TRUE]);
        }
    } else {
        return $response->withHeader("Location", "/error_internal", 403);
    }
});

// Orders history ==========================================================================================================
$app->get('/myorders[/{userId:[0-9]+}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $userId = isset($_SESSION['user']) ? $_SESSION['user']['id'] : NULL;
    //$userId = $args['userId'];
    if ($userId === NULL) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    } else {
        $orderlist = DB::query("SELECT oi.orderId as 'orderId', oi.name as 'productName', oi.productId as 'productId', oi.unitPrice as 'unitPrice', 
        oi.quantity as 'quantity', p.picture1 as 'picture', oh.orderDate as 'orderDate', oh.deliveryMethod as 'deliveryMethod', oh.totalPaid as 'totalPaid', 
        oh.firstLastName as 'userName', oh.orderStatus as 'status' FROM orders as oh INNER JOIN orderitems as oi on oh.id = oi.orderId INNER JOIN products as p ON oi.productId = p.id 
        WHERE oh.userId = %s", $userId);
        return $view->render($response, 'order_list.html.twig', ['orderList' => $orderlist]);
    }
});

// Wish List ===============================================================================================================
// AJAX call, not used directy by user
$app->post('/wishlist/add/{productId:[0-9]+}', function (Request $request, Response $response, array $args) {
    $productId = $args['productId'];
    $userId = isset($_SESSION['user']) ? $_SESSION['user']['id'] : NULL;
    if ($userId === NULL) {
        $response->getBody()->write(json_encode('You must login to create a Wishlist!')); // User not logged in
        return $response;
    }
    $productInWishList = DB::queryFirstRow("SELECT * FROM wishlist WHERE productId = %s AND userId = %s", $productId, $userId);
    if (!isset($productInWishList)) {
        DB::insert('wishlist', ['userId' => $userId, 'productId' => $productId]);
        $response->getBody()->write(json_encode('Product added to Wishlist!')); // Add product to wishlist
    } else {
        $response->getBody()->write(json_encode('Product already in Wishlist!')); // Already in wishlist
    }
    return $response;
});

// AJAX call, not used directy by user
$app->get('/wishlist/getquantity', function (Request $request, Response $response, array $args) {
    $userId = isset($_SESSION['user']) ? $_SESSION['user']['id'] : NULL;
    if ($userId === NULL) {
        $count = 0;
    } else {
        $count = DB::queryFirstField("SELECT COUNT(*) FROM wishlist WHERE userId = %s", $userId);
    }
    $json = json_encode($count);
    $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
    $response->getBody()->write($json);
    return $response;
});

$app->get('/mywishlist[/{userId:[0-9]+}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $userId = isset($_SESSION['user']) ? $_SESSION['user']['id'] : NULL;
    //$userId = $args['userId'];
    if ($userId === NULL) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    } else {
        $wishlist = DB::query("SELECT wl.productId as 'id', p.name as 'name', p.category as 'category', p.unitPrice as 'unitPrice', p.picture1 as 'picture' FROM wishlist as wl INNER JOIN products as p ON wl.productId = p.id WHERE wl.userId = %s", $userId);
        return $view->render($response, 'wishlist.html.twig', ['wishlist' => $wishlist]);
    }
});

$app->get('/mywishlist/{userId:[0-9]+}/delete/{productId:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $userId = isset($_SESSION['user']) ? $_SESSION['user']['id'] : NULL;
    if ($userId === NULL) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    }
    $productId = $args['productId'];
    if ($productId === NULL) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    } else {
        $where = new WhereClause('and'); // create a WHERE statement of pieces joined by ANDs
        $where->add('productId=%s', $productId);
        $where->add('userId=%s', $userId);
        DB::query("DELETE FROM wishlist WHERE %l", $where);
        $response = $response->withStatus(302);
        return $response->withHeader('Location', '/mywishlist/' . $userId);
    }
});

// Reviews ================================================================================================================
$app->map(['GET', 'POST'], '/review/write/{id:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $userId = isset($_SESSION['user']) ? $_SESSION['user']['id'] : NULL;
    if ($userId === NULL) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    }
    $productId = $args['id'];
    $product = DB::queryFirstRow("SELECT * FROM products WHERE id = %s", $productId);
    if ($product === NULL) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    }
    $review = DB::queryFirstRow("SELECT * FROM reviews WHERE productId = %s AND userId = %s", $productId, $userId);
    if ($review) {
        return $view->render($response, 'review_write_notallowed.html.twig');
    }
    if ($request->getMethod() == 'POST') {
        $postVars = $request->getParsedBody();
        $rating = $postVars['rating'];
        $content = $postVars['content'];
        $messages = array();
        if ($rating == null || $rating < 1 || $rating > 5) {
            $messages['rating'] = "You must use stars to rate this game";
        }
        if ($content == null || strlen($content) < 10 || strlen($content) > 1000) {
            $messages['content'] = "Content must be 10-1000 characters long";
        }
        if ($messages) {
            return $view->render($response, 'review_write.html.twig', ['product' => $product, 'messages' => $messages, 'v' => $postVars]);
        } else {
            DB::insert('reviews', ['productId' => $productId, 'userId' => $userId, 'rating' => $rating, 'content' => $content]);
            return $view->render($response, 'review_write_success.html.twig');
        }
    } else {
        return $view->render($response, 'review_write.html.twig', ['product' => $product]);
    }
});

$app->get('/myreviews[/{userId:[0-9]+}]', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $userId = isset($_SESSION['user']) ? $_SESSION['user']['id'] : NULL;
    if ($userId === NULL) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    } else {
        $reviews = DB::query("SELECT r.id as 'reviewId', r.productId as 'productId', r.userId as 'userId', r.rating as 'rating', 
        r.content as 'content', r.creationTS as 'creationTS', p.name as 'productName', p.picture1 as 'productPicture'
        FROM users as u INNER JOIN reviews as r on u.id = r.userId INNER JOIN products as p ON r.productId = p.id 
        WHERE r.userId = %s", $userId);
        $reviewList = array();
        foreach ($reviews as &$review) {
            $reviewId = $review['reviewId'];
            $productId = $review['productId'];
            $rating = $review['rating'];
            $content = strip_tags($review['content'], "<p><ul><li><ol><em><strong><i><b><h3><h4><span>");
            $creationTS = $review['creationTS'];
            $date = date('M d, Y', strtotime($creationTS));
            $time = date('H:i', strtotime($creationTS));
            $productName = $review['productName'];
            $productPicture = $review['productPicture'];     
            array_push($reviewList, ['reviewId' => $reviewId, 'productId' => $productId, 'rating' => $rating , 'content' => $content, 'date' => $date, 'time' => $time, 'productName' => $productName, 'productPicture' =>  $productPicture]);
        }
        return $view->render($response, 'reviews_list.html.twig', ['reviewList' => $reviewList]);
    }
});

$app->get('/myreviews/{userId:[0-9]+}/delete/{reviewId:[0-9]+}', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    $userId = isset($_SESSION['user']) ? $_SESSION['user']['id'] : NULL;
    if ($userId === NULL) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    }
    $reviewId = $args['reviewId'];
    if ($reviewId === NULL) {
        return $response->withHeader('Location', '/error_notfound')->withStatus(302);
    } else {
        DB::query("DELETE FROM reviews WHERE id=%s", $reviewId);
        $response = $response->withStatus(302);
        return $response->withHeader('Location', '/myreviews/' . $userId);
    }
});

// LOGOUT =================================================================================================================
$app->get('/logout', function (Request $request, Response $response, array $args) {
    $view = Twig::fromRequest($request);
    unset($_SESSION['user']);
    unset($_SESSION['provider']);
    return $view->render($response, 'logout.html.twig');
});

function moveUploadedFile($username, $directory, UploadedFileInterface $uploadedFile)
{
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = strtolower($username);
    $filename = sprintf('%s.%0.8s', $basename, $extension);
    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);
    $filepath = "/uploads/" . $filename;
    return $filepath;
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function sendConfirmationEmail($email, $secret, $username)
{
    global $log;
    $emailBody = file_get_contents('templates/register_confirmation_email.html.strsub');
    $emailBody = str_replace('EMAIL', $email, $emailBody);
    $emailBody = str_replace('SECRET', $secret, $emailBody);
    $config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey(
        'api-key',
        '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'
    );
    $apiInstance = new SendinBlue\Client\Api\SMTPApi(new GuzzleHttp\Client(), $config);
    // \SendinBlue\Client\Model\SendSmtpEmail | Values to send a transactional email
    $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
    $sendSmtpEmail->setSubject("Confirmation email for Game Store");
    $sendSmtpEmail->setSender(new \SendinBlue\Client\Model\SendSmtpEmailSender(
        ['name' => 'No-Reply', 'email' => 'noreply@teacher.ip20.com']
    ));
    $sendSmtpEmail->setTo([new \SendinBlue\Client\Model\SendSmtpEmailTo(
        ['name' => $username, 'email' => $email]
    )]);
    $sendSmtpEmail->setHtmlContent($emailBody);
    try {
        $result = $apiInstance->sendTransacEmail($sendSmtpEmail);
        $log->debug(sprintf("Confirmation email sent to %s", $email));
        $isEmailSent = true;
    } catch (Exception $e) {
        $log->error(sprintf("Error sending confirmation email to %s\n:%s", $email, $e->getMessage()));
        $isEmailSent = false;
    }
    return $isEmailSent;
}

function sendPasswordReset($email, $secret, $user)
{
    global $log;
    $emailBody = file_get_contents('templates/password_reset_email.html.strsub');
    $emailBody = str_replace('EMAIL', $email, $emailBody);
    $emailBody = str_replace('SECRET', $secret, $emailBody);
    $config = SendinBlue\Client\Configuration::getDefaultConfiguration()->setApiKey(
        'api-key',
        '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'
    );
    $apiInstance = new SendinBlue\Client\Api\SMTPApi(new GuzzleHttp\Client(), $config);
    // \SendinBlue\Client\Model\SendSmtpEmail | Values to send a transactional email
    $sendSmtpEmail = new \SendinBlue\Client\Model\SendSmtpEmail();
    $sendSmtpEmail->setSubject("Password Reset from Game Store");
    $sendSmtpEmail->setSender(new \SendinBlue\Client\Model\SendSmtpEmailSender(
        ['name' => 'No-Reply', 'email' => 'noreply@gamestore.com']
    ));
    $sendSmtpEmail->setTo([new \SendinBlue\Client\Model\SendSmtpEmailTo(
        ['name' => $user['name'], 'email' => $email]
    )]);
    $sendSmtpEmail->setHtmlContent($emailBody);
    try {
        $result = $apiInstance->sendTransacEmail($sendSmtpEmail);
        $log->debug(sprintf("Password reset sent to %s", $email));
        $isEmailSent = true;
    } catch (Exception $e) {
        $log->error(sprintf("Error sending password reset to %s\n:%s", $email, $e->getMessage()));
        $isEmailSent = false;
    }
    return $isEmailSent;
}

function verifyUsernameQuality($username)
{
    if (preg_match('/^[a-zA-Z0-9_]{5,50}$/', $username) != 1) {
        return "Username must be 5-50 characters long, and only consist of letters, digits, and underscores";
    } else { // check if username exist on database
        $user = DB::queryFirstRow("SELECT * FROM users WHERE username=%s", $username);
        if ($user) {
            return "Username already registered, try a different one";
        }
    }
    return TRUE;
}

function verifyNameQuality($name)
{
    if (preg_match('/^[a-zA-Z ]{5,100}$/', $name) != 1) {
        return "Name must be 5-100 characters long, and only consist of uppercase/lowercase letters";
    }
    return TRUE;
}

function verifyPasswordQuality($password)
{
    if (strlen($password) < 6 || strlen($password) > 100) return "Password must be 6~100 characters long";
    if (preg_match("/[a-z]/", $password) == false) return "Password must contain at least one uppercase letter";
    if (preg_match("/[A-Z]/", $password) == false) return "Password must contain at least one lower case letter";
    if (preg_match("/[0-9#$%^&*()+=-\[\]';,.\/{}|:<>?~]/", $password) == false) return "Password must contain at least one number or special character";
    else return TRUE;
}

function verifyEmailQuality($email)
{
    if (filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
        return "Invalid email";
    }
    // check if email exist on database
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    if ($user !== NULL && $user['secret'] === null) { // 
        return "Email already registered, try a different one";
    }
    if ($user !== NULL && $user['secret'] !== null) { // email already registered but not confirmed
        // create new secret and resend confirmation email
        $secret = generateRandomString(60);
        $creationDateTime = gmdate("Y-m-d H:i:s"); // GMT time zone
        DB::update('users', ['secret' => $secret, 'creationDateTime' => $creationDateTime], "id=%d", $user['id']);
        sendConfirmationEmail($email, $secret, $user['username']);
        return 'emailNotConfirmed';
    }
    return TRUE;
}