<?php

use DI\Container;
use Psr\Http\Message\UploadedFileInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Hybridauth\Exception\Exception;
use Hybridauth\Hybridauth;
use Hybridauth\HttpClient;
use Hybridauth\Storage\Session;

require __DIR__ . '/vendor/autoload.php';
require_once 'hybridconfig.php';

session_start();

// Create App and Container
$container = new Container();
$container->set('upload_directory', __DIR__ . '/uploads');
AppFactory::setContainer($container);
$app = AppFactory::create(); // call to a static method (factory method)

// Add Error Middleware for 404 - not found handling
$errorMiddleware = $app->addErrorMiddleware(true, true, true);

$errorMiddleware->setErrorHandler(
    \Slim\Exception\HttpNotFoundException::class, 
        function () use ($app) {
            $response = $app->getResponseFactory()->createResponse();
            $response = $response->withHeader('Content-type', 'application/json; charset=UTF-8');
            $response = $response->withStatus(404);
            $response->getBody()->write(json_encode("404 - not found"));
            return $response;
        }
);

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG)); // everything from debug to higher
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR)); // error or higher

// To use stripe:
require_once('vendor/autoload.php');  // To use the stripe SDK for php
\Stripe\Stripe::setApiKey('sk_test_x0ZuzruxekfNmMpavK1z8pYW004uA5HF1A'); // server key (secret key) from the account created in stripe    


// Create database connection
//For the server:
if (strpos($_SERVER['HTTP_HOST'], "ipd20.com") !== false) {  // hosting on ipd20.com
    DB::$user = 'cp4966_gamestore';        
    DB::$password = 'KJS4dVrCsyCCg9HN';
    DB::$dbName = 'cp4966_gamestore';
} else {    // local computer
    DB::$user = 'gamestore';
    DB::$password = 'KJS4dVrCsyCCg9HN';
    DB::$dbName = 'gamestore';
    DB::$port = 3333;
    //DB::$host = '127.0.0.1';    // Mac machines may need this line
}

DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)

function db_error_handler($params)
{
    header("Location: /error_internal"); // raw way to redirect a page
    global $log; // access log as global, and not as a local variable
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    die;
}

// Create Twig
$twig = Twig::create(__DIR__ . '/templates', ['cache' => __DIR__ . '/cache', 'debug' => true]);

// Set Global variables
$_SESSION['id'] = session_id();
$twig->getEnvironment()->addGlobal('sessionId', isset($_SESSION['id']) ? $_SESSION['id'] : false);
$twig->getEnvironment()->addGlobal('userSession', isset($_SESSION['user']) ? $_SESSION['user'] : false);
$twig->getEnvironment()->addGlobal('config', $config);
$twig->getEnvironment()->addGlobal('provider', isset($_SESSION['provider']) ? $_SESSION['provider'] : false);
$twig->getEnvironment()->addGlobal('adapter', isset($_SESSION['adapter']) ? $_SESSION['adapter'] : false);

//Feed configuration array to Hybridauth and instantiate the class
$hybridauth = new Hybridauth($config);

// Add Twig-View Middleware
$app->add(TwigMiddleware::create($app, $twig));