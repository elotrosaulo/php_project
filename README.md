# About

The purpose of this project was to develop a Game Store Website. The store is specialized in video games, consoles and related products.

In a user-friendly way, this website provides information about games, including news, latest releases, upcoming releases, reviews, ratings, images, and videos (trailers,gameplays, etc.).

Users can easily create an account, login with social media, search for games, purchase products, as well as write reviews and rate the products.

# Technologies used

HTML, JS, JQuery, and most of all PHP Programming.

Design in Visual Studio Code using external libraries and tools as Composer, Twig, Slim, MeekroDB, Bootstrap, Sendinblue, Hybridauth, Stripe, Unirest.

# Team 

Website developed by [Giovana de Moraes Ourique](https://www.linkedin.com/in/giovana-de-moraes-ourique-2a5732136/) and [Saulo Carranza](https://www.linkedin.com/in/saulocarranza/), as a part of the IPD-20 program.

# Link

[Game Store](https://gamestore.ipd20.com/)

